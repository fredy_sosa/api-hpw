const restify = require('restify'),
	server = restify.createServer(),
  mainRoutes = require('./routes/index'),
  actividad = require('./routes/actividades'),
  institucion = require('./routes/instituciones'),
  localidad = require('./routes/localidades'),
  municipio = require('./routes/municipios'),
  Router = require('restify-router').Router,
  router = new Router(),
  bd = require("./connection"),
  fk = require("./db");

server.use(restify.bodyParser({ mapParams: false }));
server.use(restify.queryParser({mapParams: false}));

router.add('/',mainRoutes);
router.add('/actividades',actividad);
router.add('/instituciones',institucion);
router.add('/localidades',localidad);
router.add('/municipios',municipio);

router.applyRoutes(server);

server.listen(8080, function() {
  console.log('%s listening at %s', server.name, server.url);
});