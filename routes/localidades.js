const Router = require('restify-router').Router,
	router = new Router(),
	parser = require('js2xmlparser2'),
	Localidad = require('../modelos/localidad'),
	Municipio= require('../modelos/municipio'),
	sequelize = require('../connection'),
	CbController=require('../funciones/cabecera'),
	cb=new CbController();

router.get('/',function(req,res,next){
	let pagina = cb.validaPagina(req.query.pagina);
	let limite = cb.validaLimite(req.query.limite,30);
	let name = cb.validaNombre(req.query.nombre);
	if(name&&pagina!=null&&limite!=null){
		let nombre= name.toString().split(',');
		let respuesta={localidad:[]};
		Localidad.findAll({
			offset: limite*pagina,
			where:{
				nombre:{
					$ilike: ('%'+nombre[0]+'%')
				}
			}
		}).then(datos=>{
			if(datos.length>0){
				for(let i=0;i<limite;i++){
					if(i<datos.length)
						respuesta.localidad.push({
							id:datos[i].id,
							clavelocalidad:datos[i].clavelocalidad,
							nombre:datos[i].nombre,
							url:`/localidades/${datos[i].id}`
						});
					else
						break;
				}
				if(datos.length>limite){
					respuesta.links=new Object();
					respuesta.links.siguiente=(name=='%%')?
					`/localidades?pagina=${pagina+2}&limite=${limite}`:
					`/localidades?pagina=${pagina+2}&limite=${limite}&nombre=${nombre[0]}`;
				}
				if(pagina>0){
					if(!respuesta.links)
						respuesta.links=new Object();
						respuesta.links.anterior=(name=='%%')?
						`/localidades?pagina=${pagina}&limite=${limite}`:
						`/localidades?pagina=${pagina}&limite=${limite}&nombre=${nombre[0]}`;
				}
				switch(cb.validarCb(req.headers.accept)){
					case 'application/xml':
						res.setHeader('Content-Type', 'application/xml');
						res.status(200);
						res.end(parser("localidades",respuesta,{attributeString : "links",useCDATA:true}));
						break;	
					case 'application/json':
						res.setHeader('Content-Type', 'application/json');
						res.json(200,respuesta);	
						break;
					default:
						res.status(406);
						res.end();
						break;
				}
			}else{
				res.status(404);
				res.end();
			}
		});
	}else{
		res.status(400);
		res.end();
	}
	next();
});

router.get("/:id",function(req,res,next){
	if(parseInt(req.params.id)){
		Localidad.findById(
				req.params.id
		).then(datos=>{
		let localidad={
				id:datos.id,
				clavelocalidad:datos.clavelocalidad,
				nombre:datos.nombre,
				cp:datos.codigopostal,
				url:`/localidades/${datos.id}`,
				instituciones:`/localidades/${datos.id}/instituciones`,
				actividades:`/localidades/${datos.id}/actividades`
			};
		switch(cb.validarCb(req.headers.accept)){
			case 'application/xml':
				res.setHeader('Content-Type', 'application/xml');
				res.status(200);
				res.end(parser("localidad",localidad,{useCDATA:true}));
				break;	
			case 'application/json':
				res.setHeader('Content-Type', 'application/json');
				res.json(200,{localidad:localidad});	
				break;
			default:
				res.status(406);
				res.end();
				break;
		}
		next();
		}).catch(err=>{
			res.status(404);
			res.end();
			next();	
		});
	}else{
		res.status(404);
		res.end();
		next();
	}
});

router.get("/:id/instituciones",function(req,res,next){
	let pagina = cb.validaPagina(req.query.pagina);
	let limite = cb.validaLimite(req.query.limite,8);
	let name = cb.validaNombre(req.query.nombre);
	if(name&&pagina!=null&&limite!=null){
		let nombre= name.toString().split(',');
		let respuesta={institucion:[]};
		if(parseInt(req.params.id)){
			sequelize.query(`select i.cveinstitucion as idinst,i.nombre as nombreactividad,l.idmunicipio,l.nombre from localidad as l inner join direccion as d on d.idlocalidad=l.id inner join institucion as i on d.idinstitucion=cveinstitucion where l.id=:id and i.nombre ilike :nombreinstitucion offset ${limite*pagina};`,
				{ replacements:{id:req.params.id,nombreinstitucion:('%'+nombre[0]+'%')}, type: sequelize.QueryTypes.SELECT,offset: limite*pagina,limit: limite})
			.then(datos=>{
				if(datos.length>0){
					for(let i=0;i<limite;i++){
						if(i<datos.length)
							respuesta.institucion.push({
								cveinstitucion:datos[i].idinst,
								nombre:datos[i].nombreactividad,
								url:`/instituciones/${datos[i].idinst}`
							});
						else
							break;
					}
					if(datos.length>limite){
						respuesta.links=new Object();
						respuesta.links.siguiente=(name=='%%')?
						`/localidades/${req.params.id}/instituciones?pagina=${pagina+2}&limite=${limite}`:
						`/localidades/${req.params.id}/instituciones?pagina=${pagina+2}&limite=${limite}&nombre=${nombre[0]}`;
					}

					if(pagina>0){
						if(!respuesta.links)
							respuesta.links=new Object();
						respuesta.links.anterior=(name=='%%')?
						`/localidades/${req.params.id}/instituciones?pagina=${pagina}&limite=${limite}`:
						`/localidades/${req.params.id}/instituciones?pagina=${pagina}&limite=${limite}&nombre=${nombre[0]}`;
					}
					switch(cb.validarCb(req.headers.accept)){
						case 'application/xml':
							res.setHeader('Content-Type', 'application/xml');
							res.status(200);
							res.end(parser("instituciones",respuesta,{attributeString : "links",useCDATA:true}));
							break;	
						case 'application/json':
							res.setHeader('Content-Type', 'application/json');
							res.json(200,respuesta);	
							break;
						default:
							res.status(406);
							res.end();
							break;
					}
				}else{
					res.status(404);
					res.end();
				}
			})
		}else{
			res.status(404);
			res.end();
		}
	}else{
		res.status(400);
		res.end();
	}
});

router.get("/:id/instituciones/:claveinstitucion",function(req,res,next){
	if(parseInt(req.params.id)){
		Localidad.findOne({
			where:{
				id:req.params.id
			},
			include:[{
				model: Direccion,
				include:[{
					model: Institucion,
					where:{
					cveinstitucion:req.params.claveinstitucion
				}
				}]
			}]
		}).then(datos=>{
			
		let institucion={
				id:datos.direccions[0].institucion.cveinstitucion,
				nombre:datos.direccions[0].institucion.nombre,
				razonsocial:datos.direccions[0].institucion.razonsocial,
				url:`/instituciones/${datos.direccions[0].institucion.cveinstitucion}`
			};
		switch(cb.validarCb(req.headers.accept)){
			case 'application/xml':
				res.setHeader('Content-Type', 'application/xml');
				res.status(200);
				res.end(parser("institucion",institucion,{useCDATA:true}));
				break;	
			case 'application/json':
				res.setHeader('Content-Type', 'application/json');
				res.json(200,{institucion:institucion});	
				break;
			default:
				res.status(406);
				res.end();
				break;
		}
		next();
		}).catch(err=>{
			res.status(404);
			res.end();
			next();	
		});
	}else{
		res.status(404);
		res.end();
		next();
	}
});

router.get("/:id/actividades",function(req,res,next){
	let pagina = cb.validaPagina(req.query.pagina);
	let limite = cb.validaLimite(req.query.limite,30);
	let name = cb.validaNombre(req.query.nombre);
	if(name&&pagina!=null&&limite!=null){
		let nombre= name.toString().split(',');
		let respuesta={actividad:[]};
		if(parseInt(req.params.id)){
			sequelize.query(`select distinct a.idact,a.nomact as nombreactividad from localidad as l inner join direccion as d on d.idlocalidad=l.id inner join institucion as i on d.idinstitucion=cveinstitucion inner join actividad as a on a.idact=i.idactividad where l.idmunicipio=:id and a.nomact ilike :nombreactividad order by idact,nombreactividad offset ${limite*pagina};`,
				{ replacements:{id:req.params.id,nombreactividad:('%'+nombre[0]+'%')}, type: sequelize.QueryTypes.SELECT })
			.then(datos=>{
				if(datos.length>0){
					for(let i=0;i<limite;i++){
						if(i<datos.length)
							respuesta.actividad.push({
								id:datos[i].idact,
								nombre:datos[i].nombreactividad,
								url:`/actividades/${datos[i].idact}`
							});
						else
							break;
					}
					if(datos.length>limite){
						respuesta.links = new Object();
						respuesta.links.siguiente=(name=='%%')?
						`/localidades/${req.params.id}/actividades?pagina=${pagina+2}&limite=${limite}`:
						`/localidades/${req.params.id}/actividades?pagina=${pagina+2}&limite=${limite}&nombre=${nombre[0]}`;
					}
					if(pagina>0){
						if(!respuesta.links)
							respuesta.links = new Object();
						respuesta.links.anterior=(name=='%%')?
						`/localidades/${req.params.id}/actividades?pagina=${pagina}&limite=${limite}`:
						`/localidades/${req.params.id}/actividades?pagina=${pagina}&limite=${limite}&nombre=${nombre[0]}`;
					}
					switch(cb.validarCb(req.headers.accept)){
						case 'application/xml':
							res.setHeader('Content-Type', 'application/xml');
							res.status(200);
							res.end(parser("actividades",respuesta,{attributeString : "links",useCDATA:true}));
							break;	
						case 'application/json':
							res.setHeader('Content-Type', 'application/json');
							res.json(200,respuesta);	
							break;
						default:
							res.status(406);
							res.end();
							break;
					}
				}else{
					res.status(404);
					res.end();
				}
			})
		}else{
			res.status(404);
			res.end();
		}
	}else{
		res.status(400);
		res.end();
	}
});

router.post("/",function(req,res,next){
	if(req.body.nombre!="" && req.body.nombre!=undefined 
		&& parseInt(req.body.clavelocalidad) && parseInt(req.body.codigopostal) 
		&& parseInt(req.body.idmunicipio)){
		return sequelize.transaction(t=>{
			return Localidad.create({
				clavelocalidad : req.body.clavelocalidad,
				nombre : req.body.nombre,
				codigopostal : req.body.codigopostal,
				idmunicipio: req.body.idmunicipio
			},{transaction:t});
		}).then(resultado=>{
			res.status(201);
			res.end();
			next();
		}).catch(err=>{
			res.status(400);
			res.end();
			next();
		});
	}else{
		res.status(400);
		res.end();
		next();
	}
});

router.put("/:id",function(req,res,next){
	if(req.body.nombre!=undefined || req.body.codigopostal!=undefined || req.body.idmunicipio!=undefined){
		return sequelize.transaction(t=>{
			if(req.body.nombre!=undefined){
				return Localidad.update({
					nombre:((req.body.nombre!=undefined)?req.body.nombre:this.nombre),
					codigopostal:((req.body.codigopostal!=undefined)?req.body.codigopostal:this.codigopostal),
					idmunicipio:((req.body.idmunicipio!=undefined)?req.body.idmunicipio:this.idmunicipio)
				},{
					where:{
						id:req.params.id
					}
				},{
					returning: true,
					plain: true,
					transaction:t
				});
			}
		}).then(resultado=>{
			res.status(201);
			res.end();
			next();
		}).catch(err=>{
			res.status(400);
			res.end();
			next();
		});
	}else{
		res.status(400);
		res.end();
		next();
	}
});

router.del("/:id",function(req,res,next){
	if(parseInt(req.params.id)){
		return sequelize.transaction(t=>{
			return Localidad.destroy({
				where:{
					id:req.params.id
				}
			},{
				transaction:t
			});
		}).then(resultado=>{
			res.status(204);
			res.end();
			next();
		}).catch(err=>{
			res.status(400);
			res.end();
			next();
		});
	}else{
		res.status(400);
		res.end();
		next();
	}
});

module.exports = router;
