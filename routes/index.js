const Router = require('restify-router').Router,
	router = new Router(),
	js2xmlparser = require('js2xmlparser2');

router.get('/',function(req,res,next){
	let cuerpo_del_mensaje = {
    	"mensaje": "Hola mundo"
	}
	res.setHeader('Content-Type', 'application/json');
	res.send(cuerpo_del_mensaje);
	//res.send(js2xmlparser.parse("objeto", cuerpo_del_mensaje));
	next();
});

module.exports = router;