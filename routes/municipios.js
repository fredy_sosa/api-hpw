const Router = require('restify-router').Router,
	router = new Router(),
	parser = require('js2xmlparser2'),
	Municipio = require('../modelos/municipio');
	Localidad = require('../modelos/localidad');
	Actividad = require('../modelos/actividad');
	Institucion = require('../modelos/institucion')
	Direccion = require('../modelos/direccion')
	sequelize = require('../connection');
	Localidad = require('../modelos/localidad'),
	Actividad = require('../modelos/actividad'),
	Institucion = require('../modelos/institucion'),
	Direccion = require('../modelos/direccion'),
	sequelize=require('../connection'),
	CbController=require('../funciones/cabecera'),
	cb=new CbController();

router.get("/",function(req,res,next){
	let pagina = cb.validaPagina(req.query.pagina);
	let limite = cb.validaLimite(req.query.limite,100);
	let name = cb.validaNombre(req.query.nombre);
	if(name&&pagina!=null&&limite!=null){
		let respuesta={municipio:[]};
		let nombre= name.toString().split(',');
	 	Municipio.findAll({
			offset: pagina*limite,
			where:{
				nombre:{
					$ilike: ('%'+nombre[0]+'%')
				}
			}
	 	}).then(datos=>{
	 		if(datos.length>0){
	 			for(let i=0;i<limite;i++){
	 				if(i<datos.length)
		 				respuesta.municipio.push({
		 					id:datos[i].idmunicipio,
		 					nombre:datos[i].nombre,
		 					url:`/municipios/${datos[i].idmunicipio}`
		 				});
		 			else
		 				break;
	 			}
	 			if(datos.length>limite){
	 				respuesta.links=new Object();
			 		respuesta.links.siguiente=(name=='%%')?
			 		`/municipios?pagina=${pagina+2}&limite=${limite}`:
			 		`/municipios?pagina=${pagina+2}&limite=${limite}&nombre=${nombre[0]}`;
			 	}
			 	if(pagina>0){
			 		if(!respuesta.links)
			 			respuesta.links=new Object();
		 			respuesta.links.anterior=(name=='%%')?
			 		`/municipios?pagina=${pagina}&limite=${limite}`:
			 		`/municipios?pagina=${pagina}&limite=${limite}&nombre=${nombre[0]}`;
			 	}
				switch(cb.validarCb(req.headers.accept)){
					case 'application/xml':
						res.setHeader('Content-Type', 'application/xml');
						res.status(200);
						res.end(parser("municipios",respuesta,{attributeString : "links",useCDATA:true}));
						break;
					case 'application/json':
						res.setHeader('Content-Type', 'application/json');
						res.json(200,respuesta);
						break;
					default:
						res.status(406);
						res.end();
						break;
				}
			}else{
				res.status(404);
				res.end();
			}
	 	});
 	}else{
 		res.status(400);
 		res.end();
 	}
 	next();
 });

router.get("/:id",function(req,res,next){
	if(parseInt(req.params.id)){
		Municipio.findById(
				req.params.id
		).then(datos=>{
			let municipio={
			id:datos.idmunicipio,
			nombre:datos.nombre,
			url:`/municipios/${datos.idmunicipio}`,
			localidades:`/municipios/${datos.idmunicipio}/localidades`,
			instituciones:`/municipios/${datos.idmunicipio}/instituciones`,
			actividades:`/municipios/${datos.idmunicipio}/actividades`
			};
		switch(cb.validarCb(req.headers.accept)){
			case 'application/xml':
				res.setHeader('Content-Type', 'application/xml');
				res.status(200);
				res.end(parser("municipio",municipio,{useCDATA:true}));
				break;
			case 'application/json':
				res.setHeader('Content-Type', 'application/json');
				res.json(200,{municipio:municipio});
				break;
			default:
				res.status(406);
				res.end();
				break;
		}
		next();
		}).catch(err=>{
			res.status(404);
			res.end();
			next();
		});
	}else{
		res.status(404);
		res.end();
		next();
	}
});

router.get("/:id/localidades",function(req,res,next){
	let pagina = cb.validaPagina(req.query.pagina);
	let limite = cb.validaLimite(req.query.limite,100);
	let name = cb.validaNombre(req.query.nombre);
	if(name&&pagina!=null&&limite!=null){
		let nombre= name.toString().split(',');
		let respuesta={localidad:[]};
		if(parseInt(req.params.id)){
			Municipio.findOne({
				where:{
					idmunicipio:req.params.id
				},
				include:[{
					model: Localidad,
					offset: limite*pagina,
					where:{
						nombre:{
							$ilike: ('%'+nombre[0]+'%')
						}
					}
				}]
			}).then(municipios=>{
				if(municipios&&municipios.localidads.length>0){
					for(let i=0;i<limite;i++){
						if(i<municipios.localidads.length)
							respuesta.localidad.push({
								id:municipios.localidads[i].id,
								clavelocalidad:municipios.localidads[i].clavelocalidad,
								nombre:municipios.localidads[i].nombre,
								url:`/localidades/${municipios.localidads[i].id}`
							});
						else
							break;
					}
					if(municipios.localidads.length>limite){
						respuesta.links=new Object();
						respuesta.links.siguiente=(name=='%%')?
						`/municipios/${req.params.id}/localidades?pagina=${pagina+2}&limite=${limite}`:
						`/municipios/${req.params.id}/localidades?pagina=${pagina+2}&limite=${limite}&nombre=${nombre[0]}`;
					}
					if(pagina>0){
						if(!respuesta.links)
							respuesta.links=new Object();
						respuesta.links.anterior=(name=='%%')?
						`/municipios/${req.params.id}/localidades?pagina=${pagina}&limite=${limite}`:
						`/municipios/${req.params.id}/localidades?pagina=${pagina}&limite=${limite}&nombre=${nombre[0]}`;
					}
					switch(cb.validarCb(req.headers.accept)){
						case 'application/xml':
							res.setHeader('Content-Type', 'application/xml');
							res.status(200);
							res.end(parser("localidades",respuesta,{attributeString : "links",useCDATA:true}));
							break;
						case 'application/json':
							res.setHeader('Content-Type', 'application/json');
							res.json(200,respuesta);
							break;
						default:
							res.status(406);
							res.end();
							break;
					}
				}else{
					res.status(404);
					res.end();
				}
			});
		}else{
			res.status(404);
			res.end();
		}
	}else{
		res.status(400);
		res.end();
	}

	next();
});

router.get("/:id/localidades/:clavelocalidad",function(req,res,next){
	if(parseInt(req.params.id)){
		Municipio.findOne({
			where:{
				idmunicipio:req.params.id
			},
			include:[{
				model: Localidad,
				where:{
					id:req.params.clavelocalidad
				}
			}]
		}).then(municipios=>{
			let localidad={
				id:municipios.localidads[0].id,
				clavelocalidad:municipios.localidads[0].clavelocalidad,
				nombre:municipios.localidads[0].nombre,
				url:`/localidades/${municipios.localidads[0].id}`,
				instituciones:`/localidades/${municipios.localidads[0].id}/instituciones`,
				actividades:`/localidades/${municipios.localidads[0].id}/actividades`
			};
		switch(cb.validarCb(req.headers.accept)){
			case 'application/xml':
				res.setHeader('Content-Type', 'application/xml');
				res.status(200);
				res.end(parser("localidad",localidad,{useCDATA:true}));
				break;
			case 'application/json':
				res.setHeader('Content-Type', 'application/json');
				res.json(200,{localidad:localidad});
				break;
			default:
				res.status(406);
				res.end();
				break;
		}
		next();
		}).catch(err=>{
			res.status(404);
			res.end();
			next();
		});
	}else{
		res.status(404);
		res.end();
		next();
	}
});

router.post("/",function(req,res,next){
	if(req.body.nombre && req.body.idmunicipio){
		return sequelize.transaction(t=>{
			return Municipio.create({
				idmunicipio:req.body.idmunicipio,
				nombre:req.body.nombre
			},{transaction:t});
		}).then(resultado=>{
			res.status(201);
			res.end();
			next();
		}).catch(err=>{
			res.status(400);
			res.end();
			next();
			throw err;
		});
	}else{
		res.status(400);
		res.end();
		next();
	}
});

router.put("/:id",function(req,res,next){
	if(parseInt(req.params.id) && req.body.nombre){
		return sequelize.transaction(t=>{
			return Municipio.update({
				nombre:req.body.nombre
			},{
				where:{
					idmunicipio:req.params.id
				}
			},{
				returning: true,
				plain: true,
				transaction:t
			});
		}).then(resultado=>{
			res.status(201);
			res.end();
			next();
		}).catch(err=>{
			res.status(400);
			res.end();
			next();
		});
	}else{
		res.status(400);
		res.end();
	}
});

router.get("/:id/actividades",function(req,res,next){
	let pagina = cb.validaPagina(req.query.pagina);
	let limite = cb.validaLimite(req.query.limite,100);
	let name = cb.validaNombre(req.query.nombre);
	if(name&&pagina!=null&&limite!=null){
		let nombre= name.toString().split(',');
		let respuesta={actividad:[]};
		if(parseInt(req.params.id)){
			sequelize.query(`select distinct a.idact,a.nomact as nombreactividad from localidad as l inner join municipio as m on m.idmunicipio=l.idmunicipio  inner join direccion as d on d.idlocalidad=l.id  inner join institucion as i on d.idinstitucion=cveinstitucion inner join actividad as a on a.idact=i.idactividad where l.idmunicipio=:id and a.nomact ilike :nombreactividad order by idact,nombreactividad offset ${limite*pagina}`,
				{ replacements:{id:req.params.id,nombreactividad:('%'+nombre[0]+'%')}, type: sequelize.QueryTypes.SELECT })
			.then(datos=>{
				if(datos.length>0){
					for(let i=0;i<limite;i++){
						if(i<datos.length)
							respuesta.actividad.push({
								id:datos[i].idact,
								nombre:datos[i].nombreactividad,
								url:`/actividades/${datos[i].idact}`
							});
					}
					if(datos.length>limite){
						respuesta.links = new Object();
						respuesta.links.siguiente=(name=='%%')?
					 	`/municipios/${req.params.id}/actividades?pagina=${pagina+2}&limite=${limite}`:
					 	`/municipios/${req.params.id}/actividades?pagina=${pagina+2}&limite=${limite}&nombre=${nombre[0]}`;
					 }
				 	if(pagina>0){
				 		if(!respuesta.links)
				 			respuesta.links = new Object();
				 		respuesta.links.anterior=(name=='%%')?
				 		`/municipios/${req.params.id}/actividades?pagina=${pagina}&limite=${limite}`:
				 		`/municipios/${req.params.id}/actividades?pagina=${pagina+2}&limite=${limite}&nombre=${nombre[0]}`;
				 	}
					switch(cb.validarCb(req.headers.accept)){
						case 'application/xml':
							res.setHeader('Content-Type', 'application/xml');
							res.status(200);
							res.end(parser("actividades",respuesta,{attributeString : "links",useCDATA:true}));
							break;
						case 'application/json':
							res.setHeader('Content-Type', 'application/json');
							res.json(200,respuesta);
							break;
						default:
							res.status(406);
							res.end();
							break;
					}
				}else{
					res.status(404);
					res.end();
				}
			})
		}else{
			res.status(404);
			res.end();
		}
	}else{
		res.status(400);
		res.end();
	}
	next();
});

router.get("/:id/instituciones",function(req,res,next){
	let pagina = cb.validaPagina(req.query.pagina);
	let limite = cb.validaLimite(req.query.limite,100);
	let name = cb.validaNombre(req.query.nombre);
	if(name&&pagina!=null&&limite!=null){
		let nombre= name.toString().split(',');
		let respuesta={institucion:[]};
		if(parseInt(req.params.id)){
			sequelize.query(`select distinct i.cveinstitucion as idinst,i.nombre as nombreinstitucion from localidad as l inner join municipio as m on m.idmunicipio=l.idmunicipio inner join direccion as d on d.idlocalidad=l.id inner join institucion as i on d.idinstitucion=cveinstitucion where l.idmunicipio=:id and i.nombre ilike :nombreinstitucion order by idinst,i.nombre offset ${limite * pagina}`,
				{ replacements:{id:req.params.id,nombreinstitucion:('%'+nombre[0]+'%')}, type: sequelize.QueryTypes.SELECT })
			.then(datos=>{
				if(datos.length>0){
					for(let i=0;i<limite;i++){
						if(i<datos.length)
							respuesta.institucion.push({
								cveinstitucion:datos[i].idinst,
								nombre:datos[i].nombreinstitucion,
								url:`/instituciones/${datos[i].idinst}`
							});
					}
					if(datos.length>limite){
						respuesta.links=new Object();
						respuesta.links.siguiente=(name=='%%')?
					 	`/municipios/${req.params.id}/instituciones?pagina=${pagina+2}&limite=${limite}`:
					 	`/municipios/${req.params.id}/instituciones?pagina=${pagina+2}&limite=${limite}&nombre=${nombre[0]}`;
					}
				 	if(pagina>0){
				 		if(!respuesta.links)
				 			respuesta.links = new Object();
				 		respuesta.links.anterior=(name=='%%')?
				 		`/municipios/${req.params.id}/instituciones?pagina=${pagina}&limite=${limite}`:
				 		`/municipios/${req.params.id}/instituciones?pagina=${pagina+2}&limite=${limite}&nombre=${nombre[0]}`;
				 	}
					switch(cb.validarCb(req.headers.accept)){
						case 'application/xml':
							res.setHeader('Content-Type', 'application/xml');
							res.status(200);
							res.end(parser("instituciones",respuesta,{attributeString : "links",useCDATA:true}));
							break;
						case 'application/json':
							res.setHeader('Content-Type', 'application/json');
							res.json(200,respuesta);
							break;
						default:
							res.status(406);
							res.end();
							break;
					}
				}else{
					res.status(400);
					res.end();
				}
			})
		}else{
			res.status(404);
			res.end();
		}
	}else{
		res.status(400);
		res.end();
	}
	next();
});
router.del("/:id",function(req,res,next){
	if(parseInt(req.params.id)){
		return sequelize.transaction(t=>{
			return Municipio.destroy({
				where:{
					idmunicipio:req.params.id
				}
			},{transaction:t});
		}).then(resultado=>{
			if(resultado)
				res.status(204);
			else
				res.status(400);
			res.end();
			next();
		}).catch(err=>{
			res.status(400);
			res.end();
			next();
		});
	}else{
		res.status(400);
		res.end();
	}
});

router.get("/:id/localidades/:idlocalidad/actividades",function(req,res,next){
	let pagina = cb.validaPagina(req.query.pagina);
	let limite = cb.validaLimite(req.query.limite,100);
	let name = cb.validaNombre(req.query.nombre);
	if(name&&pagina!=null&&limite!=null){
		let respuesta={actividad:[]};
		let nombre= name.toString().split(',');
		if(parseInt(req.params.id)){
			sequelize.query(`select distinct a.idact,a.nomact as nombreactividad from localidad as l inner join municipio as m on m.idmunicipio=l.idmunicipio inner join direccion as d on d.idlocalidad=l.id inner join institucion as i on d.idinstitucion=cveinstitucion inner join actividad as a on a.idact=i.idactividad where l.idmunicipio=:id AND l.id=:idlocalidad and a.nomact ilike :nombreactividad order by idact,nombreactividad offset ${limite * pagina};`,
				{ replacements:{id:req.params.id,idlocalidad:req.params.idlocalidad,nombreactividad:('%'+nombre[0]+'%')}, type: sequelize.QueryTypes.SELECT })
			.then(datos=>{
				if(datos.length>0){
					for(let i=0;i<limite;i++){
						if(i<datos.length)
							respuesta.actividad.push({
								id:datos[i].idact,
								nombre:datos[i].nombreactividad,
								url:`/actividades/${datos[i].idact}`
							});
						else
							break;
					}
					if(datos.length>limite){
						respuesta.links=new Object();
						respuesta.links.siguiente=(name=='%%')?
					 	`/municipios/${req.params.id}/localidades?pagina=${pagina+2}&limite=${limite}`:
					 	`/municipios/${req.params.id}/localidades?pagina=${pagina+2}&limite=${limite}&nombre=${nombre[0]}`;
					 }
				 	if(pagina>0){
				 		if(!respuesta.links)
				 			respuesta.links=new Object();
				 		respuesta.links.anterior=(name=='%%')?
				 		`/municipios/${req.params.id}/localidades?pagina=${pagina}&limite=${limite}`:
				 		`/municipios/${req.params.id}/localidades?pagina=${pagina+2}&limite=${limite}&nombre=${nombre[0]}`;
				 	}
					switch(cb.validarCb(req.headers.accept)){
						case 'application/xml':
							res.setHeader('Content-Type', 'application/xml');
							res.status(200);
							res.end(parser("actividades",respuesta,{attributeString : "links",useCDATA:true}));
							break;
						case 'application/json':
							res.setHeader('Content-Type', 'application/json');
							res.json(200,respuesta);
							break;
						default:
							res.status(406);
							res.end();
							break;
					}
				}else{
					res.status(404);
					res.end();
				}
			})
		}else{
			res.status(404);
			res.end();
		}
	}else{
		res.status(400);
		res.end();
	}
});


router.get("/:id/localidades/:idlocalidad/instituciones",function(req,res,next){
	let pagina = cb.validaPagina(req.query.pagina);
	let limite = cb.validaLimite(req.query.limite,100);
	let name = cb.validaNombre(req.query.nombre);
	if(name&&pagina!=null&&limite!=null){
		let respuesta={institucion:[]};
		let nombre= name.toString().split(',');
		if(parseInt(req.params.id)){
			let x= parseInt(req.params.id)
			sequelize.query(`select distinct i.cveinstitucion as idinst,i.nombre as nombre from localidad as l inner join municipio as m on m.idmunicipio=l.idmunicipio  inner join direccion as d on d.idlocalidad=l.id inner join institucion as i on d.idinstitucion=cveinstitucion where l.idmunicipio=:id AND l.id=:idlocalidad and i.nombre ilike :nombreinstitucion order by idinst,i.nombre offset ${limite * pagina};`,
				{ replacements:{id:req.params.id,idlocalidad:req.params.idlocalidad,nombreinstitucion:('%'+nombre[0]+'%')}, type: sequelize.QueryTypes.SELECT })
			.then(datos=>{
				if(datos.length>0){
					for(let i=0;i<limite;i++){
						if(i<datos.length)
							respuesta.institucion.push({
								cveinstitucion:datos[i].idinst,
								nombre:datos[i].nombre,
								url:`/instituciones/${datos[i].idinst}`
							});
						else
							break;
					}
					if(datos.length>limite){
						respuesta.links = new Object;
						respuesta.links.siguiente=(name=='%%')?
					 	`/municipios/${req.params.id}/localidades/${req.params.idlocalidad}/instituciones?pagina=${pagina+2}&limite=${limite}`:
					 	`/municipios/${req.params.id}/localidades/${req.params.idlocalidad}/instituciones?pagina=${pagina+2}&limite=${limite}&nombre=${nombre[0]}`;
					 }
				 	if(pagina>0){
				 		if(!respuesta.links)
				 			respuesta.link=new Object();
				 		respuesta.links.anterior=(name=='%%')?
				 		`/municipios/${req.params.id}/localidades/${req.params.idlocalidad}/instituciones?pagina=${pagina}&limite=${limite}`:
				 		`/municipios/${req.params.id}/localidades/${req.params.idlocalidad}/instituciones?pagina=${pagina+2}&limite=${limite}&nombre=${nombre[0]}`;
				 	}
					switch(cb.validarCb(req.headers.accept)){
						case 'application/xml':
							res.setHeader('Content-Type', 'application/xml');
							res.status(200);
							res.end(parser("instituciones",respuesta,{attributeString : "links",useCDATA:true}));break;
						case 'application/json':
							res.setHeader('Content-Type', 'application/json');
							res.json(200,respuesta);
							break;
						default:
							res.status(406);
							res.end();
							break;
					}
				}else{
					res.status(404);
					res.end();
				}
			})
		}else{
			res.status(404);
			res.end();
		}
	}else{
		res.status(400);
		res.end();
	}
	next();
});
module.exports = router;
