const Router = require('restify-router').Router,
	router = new Router(),
	parser = require('js2xmlparser2'),
	Institucion = require('../modelos/institucion'),
	sequelize = require('../connection'),
	Direccion = require('../modelos/direccion'),
	Referencias = require('../modelos/referencia'),
	CbController=require('../funciones/cabecera'),
	cb=new CbController();

router.get('/',function(req,res,next){
	let pagina = cb.validaPagina(req.query.pagina);
	let limite = cb.validaLimite(req.query.limite,30);
	let name = cb.validaNombre(req.query.nombre);
	let razon = cb.validaNombre(req.query.razonsocial);
	if(name&&razon&&pagina!=null&&limite!=null){
		let nombre= name.toString().split(',');
		let razonsocial= razon.toString().toUpperCase().split(',');
		let respuesta={institucion:[]};
		console.log(razonsocial[0]);
		Institucion.findAll({
			offset: pagina*limite,
			where:{
				nombre:{
					$ilike: ('%'+nombre[0]+'%')
				},
				razonsocial:{
					$ilike: ('%'+razonsocial[0]+'%')
				}
			}
		}).then(datos=>{
			if(datos.length>0){
				for(let i=0;i<limite;i++){
					if(i<datos.length)
						respuesta.institucion.push({
							cveinstitucion:datos[i].cveinstitucion,
							nombre:datos[i].nombre,
							razonsocial:datos[i].razonsocial,
							url:`/instituciones/${datos[i].cveinstitucion}`	
						});
					else
						break;
				}
				if(datos.length>limite){
					respuesta.links=new Object();
					respuesta.links.siguiente=(name=='%%')?
					((razon=='%%')?
						`/instituciones?pagina=${pagina+2}&limite=${limite}`:
						`/instituciones?pagina=${pagina+2}&limite=${limite}&razonsocial=${razonsocial[0]}`):
					((razon=='%%')?
						`/instituciones?pagina=${pagina+2}&limite=${limite}&nombre=${nombre[0]}`:
						`/instituciones?pagina=${pagina+2}&limite=${limite}&nombre=${nombre[0]}&razonsocial=${razonsocial[0]}`);
				}

				if(pagina>0){
					if(!respuesta.links)
						respuesta.links=new Object();
					respuesta.links.anterior=(name=='%%')?
					((razon=='%%')?
						`/instituciones?pagina=${pagina}&limite=${limite}`:
						`/instituciones?pagina=${pagina}&limite=${limite}&razonsocial=${razonsocial[0]}`):
					((razon=='%%')?
						`/instituciones?pagina=${pagina}&limite=${limite}&nombre=${nombre[0]}`:
						`/instituciones?pagina=${pagina}&limite=${limite}&nombre=${nombre[0]}&razonsocial=${razonsocial[0]}`);
				}
				switch(cb.validarCb(req.headers.accept)){
					case 'application/xml':
						res.setHeader('Content-Type', 'application/xml');
						res.status(200);
						res.end(parser("instituciones",respuesta,{attributeString : "links",useCDATA:true}));
						break;
					case 'application/json':
						res.setHeader('Content-Type', 'application/json');
						res.json(200,respuesta);
						break;
					default:
						res.send(406).end();
				}
			}else{
				res.status(404);
				res.end();
			}
		});
	}else{
		res.status(400);
		res.end();
	}
	next();
});

router.get("/:id",function(req,res,next){
	let view = cb.validaVista(req.query.vista);
	console.log(view);
	if(parseInt(req.params.id)){
		Institucion.findById(
			req.params.id
			//include:[{
			//	model: Direccion
			//}]
		).then(datos=>{
			console.log(view);
			if(view==='intermedia' || view==='basica' || view==='completa' ){
				let institucion;
				if(view==='basica'){
					institucion={
					claveInstitucion:datos.cveinstitucion,
					nombre:datos.nombre,
					razon_social:datos.razonsocial,
					personal:datos.perocu,
					telefono:datos.telefono,
					email:datos.email,
	//				web:datos.sitio_web,
	//				tipoUniEco:datos.tipounieco,
					fecha_alta:datos.fechaalta,
					url:`/instituciones/${datos.cveinstitucion}`
					};

				}
				if(view==='intermedia'){
					institucion=datos;	
				}
				
				switch(cb.validarCb(req.headers.accept)){
					case 'application/xml':
						res.setHeader('Content-Type', 'application/xml');
						res.status(200);
						res.end(parser("institucion",institucion,{useCDATA:true}));
						break;
					case 'application/json':
						res.setHeader('Content-Type', 'application/json');
						res.json(200,{institucion:institucion});
						break;
					default:
					res.status(406);
					res.end();
					break;
					}
			}else{
				res.status(404);
				res.end();
			}

		next();
		}).catch(err=>{
			res.status(404);
			res.end();
			next();
		});
	}else{
		res.status(404);
		res.end();
		next();
	}
});

router.post("/",function(req,res,next){
	if(parseInt(req.body.cveinstitucion)){
			return sequelize.transaction(t=>{
				return Institucion.create({
					cveinstitucion:req.body.cveinstitucion,
					nombre:req.body.nombre,
	  				razonsocial:req.body.razonsocial,
	  				perocu:req.body.perocu,
	  				telefono:req.body.telefono,
	  				email:req.body.email,
	  				sitio_web:req.body.sitio_web,
	  				tipounieco:req.body.tipounieco,
	  				fechaalta:req.body.fechaalta,
	  				idactividad:req.body.idactividad
				}).then(datos=>{
					return Direccion.create({
						nomvial:req.body.direccion.nomvial,
						letraext:req.body.direccion.letraext,
						letraint:req.body.direccion.letraint,
						numext:req.body.direccion.numext,
						numint:req.body.direccion.numint,
						edificio:req.body.direccion.edificio,
						edificioe:req.body.direccion.edificioe,
						tipo_asent:req.body.direccion.tipo_asent,
						nombre_asent:req.body.direccion.nombre_asent,
						tipocencom:req.body.direccion.tipocencom,
						nomcencom:req.body.direccion.nomcencom,
						num_local:req.body.direccion.num_local,
						ageb:req.body.direccion.ageb,
						manzana:req.body.direccion.manzana,
						latitud:req.body.direccion.latitud,
						longitud:req.body.direccion.longitud,
						idvialidad:req.body.direccion.idvialidad,
						idlocalidad:req.body.direccion.idlocalidad,
						idinstitucion:req.body.cveinstitucion
					}).then(data=>{
						return Referencias.create({
							nom_v_e_1:req.body.direccion.referencias.nom_v_e_1,
							nom_v_e_2:req.body.direccion.referencias.nom_v_e_2,
							nom_v_e_3:req.body.direccion.referencias.nom_v_e_3,
							tipo_v_e_1:req.body.direccion.referencias.tipo_v_e_1,
							tipo_v_e_2:req.body.direccion.referencias.tipo_v_e_2,
							tipo_v_e_3:req.body.direccion.referencias.tipo_v_e_3,
							id_direccion:data.id
						});
					});
				});
			}).then(resultado=>{
				res.status(201);
				res.end();
				next();
			}).catch(err=>{
				res.status(400);
				res.end();
				throw err;
				next();
			});
	}else{
		res.status(400);
		res.end();
		next();
	}
});

router.put("/:id",function(req,res,next){
	if(parseInt(req.params.id)){
		return sequelize.transaction(t=>{
			if(req.body.direccion.referencias!=undefined){
				Direccion.findAll({
					where:{
						idinstitucion:req.params.id
					}
				}).then(datos=>{
					let id_direc=datos[0].id;
					Referencias.update({
						nom_v_e_1:req.body.direccion.referencias.nom_v_e_1,
						nom_v_e_2:req.body.direccion.referencias.nom_v_e_2,
						nom_v_e_3:req.body.direccion.referencias.nom_v_e_3,
						tipo_v_e_1:req.body.direccion.referencias.tipo_v_e_1,
						tipo_v_e_2:req.body.direccion.referencias.tipo_v_e_2,
						tipo_v_e_3:req.body.direccion.referencias.tipo_v_e_3
					},{
						where: {
							id_direccion: id_direc
						}
					},{
						returning: true,
						plain: true,
						transaction:t
					});
				});
			}
			if(req.body.direccion!=undefined){
				Direccion.update({
					nomvial:req.body.direccion.nomvial,
					letraext:req.body.direccion.letraext,
					letraint:req.body.direccion.letraint,
					numext:req.body.direccion.numext,
					numint:req.body.direccion.numint,
					edificio:req.body.direccion.edificio,
					edificioe:req.body.direccion.edificioe,
					tipo_asent:req.body.direccion.tipo_asent,
					nombre_asent:req.body.direccion.nombre_asent,
					tipocencom:req.body.direccion.tipocencom,
					nomcencom:req.body.direccion.nomcencom,
					num_local:req.body.direccion.num_local,
					ageb:req.body.direccion.ageb,
					manzana:req.body.direccion.manzana,
					latitud:req.body.direccion.latitud,
					longitud:req.body.direccion.longitud,
					idvialidad:req.body.direccion.idactividad,
					idlocalidad:req.body.direccion.idlocalidad
				},{
					where:{
						idinstitucion:req.params.id
					}
				},{
					returning: true,
					plain: true,
					transaction:t
				});
			}
			return Institucion.update({
				nombre:req.body.nombre,
	  			razonsocial:req.body.razonsocial,
	  			perocu:req.body.perocu,
	  			telefono:req.body.telefono,
	  			email:req.body.email,
	  			sitio_web:req.body.sitio_web,
	  			tipounieco:req.body.tipounieco,
	  			fechaalta:req.body.fechaalta,
	  			idactividad:req.body.idactividad
			},{
				where:{
					cveinstitucion:req.params.id
				}
			},{
				returning: true,
				plain: true,
				transaction:t
			});
		}).then(resultado=>{
			res.status(201);
			res.end();
			next();
		}).catch(err=>{
			res.status(400);
			res.end();
			next();
			throw err;
		});
	}else{
		res.send(400);
		res.end();
		next();
	}
});

router.del("/:id",function(req,res,next){
	if(parseInt(req.params.id)){
		return sequelize.transaction(t=>{
			return Direccion.findAll({
				where:{
					idinstitucion:req.params.id
				}
			}).then(datos=>{
				let id_direc=datos[0].id;
				return Referencias.destroy({
					where:{
						id_direccion:id_direc
					}
				}).then(data=>{
					console.log(id_direc);
					return Direccion.destroy({
						where:{
							idinstitucion:req.params.id
						}
					}).then(dato=>{
						return Institucion.destroy({
							where:{
								cveinstitucion:req.params.id
							}
						},{
							transaction:t
						});
					});
				});
			});
		}).then(resultado=>{
			res.status(204);
			res.end();
			next();
		}).catch(err=>{
			res.status(400);
			res.end();
			next();
		});
	}else{
		res.status(400);
		res.end();
		next();
	}
});

module.exports = router;
