const Sequelize = require('sequelize'),
baseDatos='hpw',
usuario='postgres',
password='',
port=5432;

const sequelize = new Sequelize(baseDatos, usuario, password, {
  dialect: 'postgres',
  port:port,
  pool: {
    max: 5,
    min: 0,
    idle: 10000
  }
});

sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });

module.exports = sequelize;
