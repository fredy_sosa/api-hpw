const xmlparser = require('js2xmlparser');

class CbController{
	validarCb(arreglo){
		var campos = arreglo.split(', ');
		var temporal;
		for(let i=0;i < campos.length;i++){
			if(campos[i]==='*/*'){
				temporal='*/*';
				break;
			}
			if(campos[i]==='application/xml'){
				temporal='application/xml';
				break;
			}
			if(campos[i]==='application/json'){
				temporal='application/json';
				break;
			}
			if((i+1)===campos.length){
				temporal='invalid';
				break;				
			}	
		};
		return temporal;
	}
	validaNombre(nombre){
		if(nombre){
			return nombre;
		}else{
			if(nombre=='')
				return null;
			else
				return '%%';
		}
	}
	validaRazon(nombre){
		if(nombre){
			return nombre;
		}else{
			if(nombre=='')
				return null;
			else
				return '%%';
		}
	}
	validaPagina(pagina){
		if(pagina)
			if(Number.isInteger(Number(pagina)))
				if(parseInt(pagina)>=1&&parseInt(pagina)<500)
					return parseInt(pagina)-1;
				else
					return null;
			else
				return null;
		else
			if(pagina=='')
				return null;
			else
				return 0;
	}
	validaLimite(limite,d){
		if(limite)
			if(Number.isInteger(Number(limite)))
				if(parseInt(limite)>=1&&parseInt(limite)<500)
					return parseInt(limite);
				else
					return null;
			else
				return null;
		else
			if(limite=='')
				return null;
			else
				return d;
	}
	validaVista(vista){
		if(vista){
			if(vista=='basica' || vista=='intermedia' || vista=='completa'){
				return vista;
			}else{
				return '400'
			}
		}else{
			if(vista==''){
				return '400'
			}else{
				return 'basica'
			}
		}
	}
}

module.exports = CbController;