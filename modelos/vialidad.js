const Sequelize = require("sequelize"),
	  connection = require("../connection");

const Vialidad = connection.define('vialidad', {
	idtipo: {
	    type: Sequelize.INTEGER,
	    primaryKey: true,
	    autoIncrement: true,
	    field:"idtipo",
	    allowNull: false,
	    validate:{
	    	min:25,
	    	not: ["[a-z]",'i'],
    		isInt: true,
	    }
	},
	tipo: {
	    type: Sequelize.STRING,
	    field:"tipo",
	    allowNull: false,
	    validate:{
	    	len: [1,50],
    		is: ["[a-z]+(\s|[a-z])*$",'i']
	    }
	},
	},{
		timestamps: false,
		freezeTableName: false,
		tableName: 'vialidad'
	}
);

module.exports =Vialidad;