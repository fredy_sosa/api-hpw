const Sequelize = require("sequelize"),
	  connection = require("../connection");

const Ocupantes = connection.define('ocupantes', {
	id: {
	    type: Sequelize.STRING,
	    primaryKey: true,
	    autoIncrement: true,
	    field:"id",
	    allowNull: false,
	    validate:{
	    	min:8,
	    	not: ["[a-z]",'i'],
    		isInt: true,
	    }
  	},
  	descripcion:{
  		type: Sequelize.STRING,
  		field: "descripciion",
  		allowNull: false,
  		validate:{
  			len: [1,25],
    		is: ["[a-z]+(\s|[a-z])*$",'i']
  		}
  	}
  	},{
		timestamps: false,
		freezeTableName: false,
		tableName: 'ocupantes'
	}
);

module.exports=Ocupantes;