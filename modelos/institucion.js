const Sequelize = require("sequelize"),
	  connection = require("../connection");

const Institucion = connection.define('institucion',{
	cveinstitucion:{
		type: Sequelize.STRING,
    	primaryKey: true,
    	autoIncrement: true,
		field:"cveinstitucion",
		allowNull: false
	},
	nombre:{
		type:Sequelize.STRING,
		field:"nombre",
		allowNull: false,
		validate:{
			len: [1,150]
		}
	},
	razonsocial:{
		type:Sequelize.STRING,
		field:"razonsocial",
		allowNull: true,
		validate:{
			len: [1,150],
    		is: ["[a-z]+(\s|[a-z])*$",'i']
		}
	},
	perocu:{
		type:Sequelize.INTEGER,
		field:"perocu",
		allowNull: false,
		validate:{
			not: ["[a-z]",'i'],
    		isInt: true
		}
	},
	telefono:{
		type:Sequelize.STRING,
		field:"telefono",
		allowNull:true,
		validate:{
			not: ["[a-z]",'i'],
			len: [10,10],
		}
	},
	email:{
		type:Sequelize.STRING,
		field:"email",
		allowNull:true,
		validate:{
			isEmail: true,
			len: [6,75]
		}
	},
	sitio_web:{
		type:Sequelize.STRING,
		field:"sitio_web",
		allowNull: true,
		validate:{
			len: [1,75]
		}
	},
	tipounieco:{
		type:Sequelize.STRING,
		field:"tipounieco",
		allowNull:false,
		validate:{
			is: ["[a-z]+(\s|[a-z])*$",'i'],
			len: [1,10]
		}
	},
	fechaalta:{
		type:Sequelize.STRING,
		field:"fechaalta",
		allowNull:false,
		validate:{
			len: [10,25]
		}
	},
	idactividad:{
		type:Sequelize.STRING,
		field:"idactividad",
		allowNull:false,
		validate:{
			not: ["[a-z]",'i'],
			isInt: true
		}
	}
},{
	timestamps: false,
  	freezeTableName: false,
  	tableName: 'institucion'
});

module.exports=Institucion;