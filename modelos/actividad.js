const Sequelize = require("sequelize"),
	  connection = require("../connection");

const Actividad = connection.define('actividad',{
	idact: {
		type: Sequelize.INTEGER,
    	primaryKey: true,
    	autoIncrement: true,
    	field:"idact",
    	allowNull: false,
    	validate:{
    		not: ["[a-z]",'i'],
    		isInt: true,
    		min: 714000
    	}
  	},
  	nombre: {
    	type: Sequelize.STRING,
    	field:"nomact",
    	allowNull: false,
    	validate:{
    		len: [1,250],
    		is: ["[a-z]+(\s|[a-z])*$",'i']
    	}
  	}
	},{
	  	timestamps: false,
		freezeTableName: false,
		tableName: 'actividad'
	}
);

module.exports =Actividad;