const Sequelize = require("sequelize"),
	  connection = require("../connection");

const Direccion = connection.define('direccion',{
	id: {
	    type: Sequelize.INTEGER,
	    primaryKey: true,
	    autoIncrement: true,
	    field:"id",
      allowNull: false
  	},
  	nomvial:{
  		type:Sequelize.STRING,
  		field:"nomvial",
      allowNull: false,
      validate:{
        len: [1,150]
      }
  	},
  	letraext:{
  		type:Sequelize.STRING,
  		field:"letraext",
      allowNull:true,
      validate:{
        len:[1,35]
      }
  	},
  	letraint:{
  		type:Sequelize.STRING,
  		field:"letraint",
      allowNull:true,
      validate: {
        len:[1,35]
      }
  	},
  	numext:{
  		type:Sequelize.INTEGER,
  		field:"numext",
      allowNull: false,
      validate:{
        len:[1,5],
        min: 0
      }
  	},
  	numint:{
  		type:Sequelize.INTEGER,
  		field:"numint",
      allowNull:false,
      validate:{
        len:[1,5],
        min: 0
      }
  	},
  	edificio:{
  		type:Sequelize.STRING,
  		field:"edificio",
      allowNull:true,
      validate:{
        len:[1,50]
      }
  	},
  	edificioe:{
  		type:Sequelize.STRING,
  		field:"edificioe",
      allowNull:true,
      validate:{
        len:[1,50]
      }
  	},
  	tipo_asent:{
  		type:Sequelize.STRING,
  		field:"tipo_asent",
      allowNull:true,
      validate:{
        len:[1,50]
      }
  	},
  	nombre_asent:{
  		type:Sequelize.STRING,
  		field:"nombre_asent",
      allowNull:false,
      validate:{
        len:[1,150]
      }
  	},
  	tipocencom:{
  		type:Sequelize.STRING,
  		field:"tipocencom",
      allowNull:true,
      validate:{
        len:[1,40]
      }
  	},
  	nomcencom:{
  		type:Sequelize.STRING,
  		field:"nomcencom",
      allowNull:true,
      validate:{
        len:[1,150]
      }
  	},
  	num_local:{
  		type:Sequelize.STRING,
  		field:"num_local",
      allowNull:true,
      validate:{
        len:[1,20]
      }
  	},
  	ageb:{
  		type:Sequelize.STRING,
  		field:"ageb",
      allowNull:false,
      validate:{
        len:[1,4]
      }
  	},
  	manzana:{
  		type:Sequelize.STRING,
  		field:"manzana",
      allowNull:false,
      validate:{
        len:[1,3]
      }
  	},
  	latitud:{
  		type:Sequelize.STRING,
  		field:"latitud",
      allowNull:false,
      validate:{
        isFloat: true,
        len:[9,20]
      }
  	},
  	longitud:{
  		type:Sequelize.STRING,
  		field:"longitud",
      allowNull:false,
      validate:{
        isFloat: true,
        len:[8,20]
      }
  	},
   idvialidad:{
     type:Sequelize.INTEGER,
     field:"idvialidad",
     allowNull:false,
      validate:{
        isInt: true
      }
   },
   idlocalidad:{
     type:Sequelize.INTEGER,
     field:"idlocalidad",
     allowNull:false,
      validate:{
        isInt: true
      }
   },
   idinstitucion:{
     type:Sequelize.STRING,
     field:"idinstitucion",
     allowNull:false,
     validate:{
      isInt: true
     }
  }
},{
	timestamps: false,
  	freezeTableName: false,
  	tableName: 'direccion'
});

module.exports=Direccion;