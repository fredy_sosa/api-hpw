const Sequelize = require("sequelize"),
	  connection = require("../connection");

const Localidad = connection.define('localidad',{
	id: {
		type: Sequelize.INTEGER,
    	primaryKey: true,
    	autoIncrement: true,
    	field:"id",
      allowNull: false
  	},
  	clavelocalidad:{
  		type: Sequelize.STRING,
  		field: "clavelocalidad",
      allowNull: false,
      validate:{
        isInt: true
      }
  	},
  	nombre:{
  		type: Sequelize.STRING,
  		field: "nombre",
      allowNull: false,
      validate:{
        len: [1,100],
        is: ["[a-z]+(\s|[a-z])*$",'i']
      }
  	},
  	codigopostal:{
  		type: Sequelize.INTEGER,
  		field: "codigopostal",
      allowNull: false,
      validate:{
        isInt: true,
        len: [5,5]
      }
  	},
   idmunicipio:{
     type: Sequelize.INTEGER,
     field: "idmunicipio",
     allowNull: false,
      validate:{
        isInt: true
      }
  	}
  },{
	  	timestamps: false,
		freezeTableName: false,
		tableName: 'localidad'
	}
);

module.exports =Localidad;