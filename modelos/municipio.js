const Sequelize = require("sequelize"),
	  connection = require("../connection");

const Municipio = connection.define('municipio', {
  idmunicipio: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true,
    field:"idmunicipio",
    allowNull: false,
    validate:{
      not: ["[a-z]",'i'],
      isInt: true,
      min: 571
    }
  },
  nombre: {
    type: Sequelize.STRING,
    field:"nombre",
    allowNull:false,
    validate:{
      len: [1,250],
      is: ["[a-z]+(\s|[a-z])*$",'i']
    }
  }
},
{
    timestamps: false,
  	freezeTableName: false,
  	tableName: 'municipio'
});

module.exports=Municipio;