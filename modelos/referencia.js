const Sequelize = require("sequelize"),
	  connection = require("../connection");

const Referencia = connection.define('referencia',{
	nom_v_e_1:{
		type: Sequelize.STRING,
		field:"nom_v_e_1",
		allowNull:true,
	    validate:{
	    	len: [1,150],
    		is: ["[a-z]+(\s|[a-z])*$",'i']
	    }
	},
	nom_v_e_2:{
		type: Sequelize.STRING,
		field:"nom_v_e_2",
		allowNull:true,
	    validate:{
	    	len: [1,150],
    		is: ["[a-z]+(\s|[a-z])*$",'i']
	    }
	},
	nom_v_e_3:{
		type: Sequelize.STRING,
		field:"nom_v_e_3",
		allowNull:true,
	    validate:{
	    	len: [1,150],
    		is: ["[a-z]+(\s|[a-z])*$",'i']
	    }
	},
	tipo_v_e_1:{
		type: Sequelize.INTEGER,
		field:"tipo_v_e_1",
		allowNull:true,
		validate:{
	    	not: ["[a-z]",'i'],
    		isInt: true,
		}
	},
	tipo_v_e_2:{
		type: Sequelize.INTEGER,
		field:"tipo_v_e_2",
		allowNull:true,
		validate:{
	    	not: ["[a-z]",'i'],
    		isInt: true,
		}
	},
	tipo_v_e_3:{
		type: Sequelize.INTEGER,
		field:"tipo_v_e_3",
		allowNull:true,
		validate:{
	    	not: ["[a-z]",'i'],
    		isInt: true,
		}
 	},
 	id_direccion:{
 		type: Sequelize.INTEGER,
 		field:"id_direccion",
		allowNull:false,
		validate:{
	    	not: ["[a-z]",'i'],
    		isInt: true,
		}
 	}
},{
	timestamps: false,
	freezeTableName: false,
	tableName: 'referencias'
});

Referencia.removeAttribute('id');

module.exports =Referencia;