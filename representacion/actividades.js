GET /actividades
{
	actividades:[
	{
		"id":"id",
		"nombre":"nombre_actividad",
		"url":"/actividades/id"
	},
	{
		"id":"id",
		"nombre":"nombre_actividad",
		"url":"/actividades/id"
	},
	...
	]
}

GET /actividades/{id_actividad}
{
	"id":"id",
	"nombre":"nombre"
}

PUT /actividades/{id_actividad}
Lo que envia el usuario
{
	"nombre":"nuevo_nombre"
}

POST /actividades
Lo que envia el usuario
{
	"id":nuevo_id
	"nombre":"nuevo_nombre"
}

DELETE /actividades/{id_actividad}
Nada; solo va a retornar un código de estado.

GET  "/actividades/{id_actividad}/instituciones"
{
	"instituciones":[
		{
			"id_institucion":"id_institucion",
			"nombre":"nombre_institucion",
			"url":"/instituciones/id_institucion"
		},
		{
			"id_institucion":"id_institucion",
			"nombre":"nombre_institucion",
			"url":"/instituciones/id_institucion"
		},
		...
	]
}

GET  "/actividades/{id_actividad}/instituciones/{id_institucion}"
{
	"id":"id_institucion",
	"nombre":"nombre_institucion",
	"url":"/instituciones/id_institucion"
}