GET /municipios
{
	"municipios": [
	{
		"id":"id",
		"nombre":"string",
		"url":"/municipios/id"
	},
	{
		"id":"id",
		"nombre":"string",
		"url":"/municipios/id"
	},
	...
	]
}


PUT /municipios/{id_municipio}
{
	"nombre":"string"
}

DELETE /municipios/{id_municipio}
Nada; solo va a retornar un código de estado.

POST /municipios
(Agregar un municipio, no venia en el documento)
{
	"nombre":"string"
}

GET /municipios/{id_municipio}
{
	"id":"id",
	"nombre":"string"
}

GET "/municipios/{id_municipio}/localidades"
{
	"localidades":[
		{
		"claveLocalidad":"id",
		"nombre":"string",
		"url":"/localidades/id"
		},
		{
		"claveLocalidad":"id",
		"nombre":"string",
		"url":"/localidades/id"
		},
		...
	]
}

GET "/municipios/{id_municipio}/localidades/{id_localidad}"
{
	"claveLocalidad":"id",
	"nombre":"string"
	"url":"/localidades/id"
}

GET "/municipios/{id_municipio}/localidades/{id_localidades}/instituciones"
{
	"instituciones":[
		{
			"id":"cveInstitucion",
			"nombre":"nombre_institucion",
			"url":"/instituciones/cveInstitucion"
		},
		{
			"id":"cveInstitucion",
			"nombre":"nombre_institucion",
			"url":"/instituciones/cveInstitucion"
		},
		...
	]
}

GET "/municipios/{id_municipio}/actividades"
{
	"actividades":[
		{
			"id":"id_actividad",
			"nombre":"nombre_actividad",
			"url":"/actividades/id_actividad"
		},
		{
			"id":"id_actividad",
			"nombre":"nombre_actividad",
			"url":"/actividades/id_actividad"
		},
		...
	]
}

GET "/municipios/{id_municipio}/instituciones"
{
	"instituciones":[
		{
			"id":"cveInstitucion",
			"nombre":"nombre_institucion",
			"url":"/instituciones/cveInstitucion"
		},
		{
			"id":"cveInstitucion",
			"nombre":"nombre_institucion",
			"url":"/instituciones/cveInstitucion"
		},
		...
	]
}

GET "/municipios/{id_municipio}/localidades/{id_localidades}/actividades"
{
	"actividades":[
		{
			"id":"id_actividad",
			"nombre":"nombre_actividad",
			"url":"/actividades/id_actividad"
		},
		{
			"id":"id_actividad",
			"nombre":"nombre_actividad",
			"url":"/actividades/id_actividad"
		},
		...
	]
}