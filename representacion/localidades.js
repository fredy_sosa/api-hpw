GET /localidades
{
	"localidades":[
		{
			"cveLocalidad":"cveLocalidad",
			"nombre":"",
			"url":"/localidades/cveLocalidad"
		},
		{
			"cveLocalidad":"cveLocalidad",
			"nombre":"",
			"url":"/localidades/cveLocalidad"
		},
		...
	]
}

GET /localidades/{id_localidad}
{
	"cveLocalidad":"cveLocalidad",
	"nombre":"nombre",
	"cp":"cp",
	"url_municipio":"/municipios/id_municipio"
}

PUT /localidades/{id_localidad}
elegir al menos uno de los campos para modificar
{
	"nombre":"nombre(opcional)",
	"cp":"cp(opcional)",
	"id_municipio":"id municipio(opcional)"
}

DELETE /localidades/{id_localidad}
Nada; solo regresa un código de estado