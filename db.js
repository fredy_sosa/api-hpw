var actividad = require('./modelos/actividad');
var direccion = require('./modelos/direccion');
var institucion = require('./modelos/institucion');
var localidad = require('./modelos/localidad');
var municipio = require('./modelos/municipio');
var ocupantes = require('./modelos/ocupantes');
var referencia = require('./modelos/referencia');
var vialidad = require('./modelos/vialidad');

actividad.hasMany(institucion,{
	foreignKey: "idactividad",
	sourceKey: "idact"
});


institucion.belongsTo(actividad,{
	foreignKey: "idactividad"	
})


municipio.hasMany(localidad,{
	foreignKey:"idmunicipio",
	sourceKey:"idmunicipio"
});

ocupantes.hasMany(institucion,{
	foreignKey: "perocu"
});

vialidad.hasMany(direccion,{
	foreignKey: "idvialidad"
});
localidad.hasMany(direccion,{
	foreignKey: "idlocalidad"
});
//institucion.hasOne(direccion,{
//	foreignKey: "idinstitucion",
//	sourceKey: "cveinstitucion"
//})
direccion.belongsTo(institucion,{
	foreignKey: "idinstitucion"
});
vialidad.hasMany(referencia,{
	foreignKey: "tipo_v_e_1"
});
vialidad.hasMany(referencia,{
	foreignKey: "tipo_v_e_2"
});
vialidad.hasMany(referencia,{
	foreignKey: "tipo_v_e_3"
});
referencia.belongsTo(direccion,{
	foreignKey: "iddireccion"
});
