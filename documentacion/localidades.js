/**
    @apiDefine 404
    @apiError NotFound <code>404</code> No se encontró el recurso especificado
*/
/**
    @apiDefine 406
    @apiError NotAcceptable <code>406</code> No se especifico una cabecera Accept o no es XML o JSON
*/
/**
    @apiDefine 400
    @apiError BadRequest <code>400</code> Alguno de los parametros no tiene un valor valido.
*/
/**
  @api {get} /localidades Solicitud para obtener todas las localidades del estado de Oaxaca
  @apiName GetLocalidades
  @apiGroup Localidades

  @apiParam {String} [nombre] Nombre de la localidad, usado para filtrar la respuesta
  @apiParam {Number} [limite=10] Total de resultados, debe ser mayor a 1 y menor 500
  @apiParam {Number} [pagina=1] Utilizado para obtener el offset de los resultados, se usa en conjunto con el parametro limite,debe ser mayor a 1 y menor 30 =1

  @apiSuccess {Object[]} localidades Lista de localidades.
  @apiSuccess {Object} [links] URL's a datos relacionados con las localidades.
  @apiSuccess {Number} localidades.id  Identificador unico de la localidad.
  @apiSuccess {Number} localidades.cveLocalidad Clave de la localidad.
  @apiSuccess {String} localidades.nombre  Nombre de la actividad.
  @apiSuccess {Number} localidades.url  URL para acceder a la informacion detallada de la localidad.
    @apiSuccess {String} links.anterior URL URL para acceder a la pagina anterior de la respuesta solo se devuelve si la pagina es diferente a 1.
    @apiSuccess {String} links.siguiente URL para acceder a la siguiente pagina de la respuesta.
  @apiSuccessExample {json} JSON:
      HTTP/1.1 200 OK
      {
        "localidades": [
                        {
                          "id": 1,
                          "clavelocalidad": 1,
                          "nombre": "San Lorenzo Cuaunecuiltitla",
                          "url": "/localidades/1"
                        },
                        {
                          "id": 2,
                          "clavelocalidad": 1,
                          "nombre": "Asunción Nochixtlán",
                          "url": "/localidades/2"
                        }
                      ]
        "links":  {
                    "siguiente": "/localidades?pagina=2&limite=100"
                  }
      }
  @apiSuccessExample {xml} XML
      HTTP/1.1 200 OK
      <?xml version="1.0" encoding="UTF-8"?>
      <localidades siguiente="/localidades?pagina=2&amp;limite=100">
        <localidad>
          <id>1</id>
          <clavelocalidad>1</clavelocalidad>
          <nombre>San Lorenzo Cuaunecuiltitla</nombre>
          <url>/localidades/1</url>
        </localidad>
        <localidad>
          <id>2</id>
          <clavelocalidad>1</clavelocalidad>
          <nombre>Asunción Nochixtlán</nombre>
          <url>/localidades/2</url>
        </localidad>
      </localidades>
  @apiUse 406
  @apiUse 400
  */

  /**
  @api {get} /localidades/:id Solicitud para obtener una localidades, especificada por su identificador unico
  @apiName GetLocalidad
  @apiGroup Localidades

  @apiParam {Number} id Identificador de la localidad

  @apiSuccess {Number} id  Identificador unico de la localidad.
  @apiSuccess {Number} claveLocalidad  Clave de la localidad.
  @apiSuccess {String} nombre  Nombre de la localidad.
  @apiSuccess {Number} cp  Codigo postal de la localidad.
  @apiSuccess {String} url  URL para acceder a la informacion detallada de la localidad.
  @apiSuccess {String} instituciones  URL para acceder a la informacion detallada de instituciones ubicadas en la localidad.
  @apiSuccess {String} actividades  URL para acceder a la informacion detallada de actividades ubicadas en la localidad.

  @apiSuccessExample {json} JSON:
      HTTP/1.1 200 OK
      {
        "id": 1,
        "clavelocalidad": 1,
        "nombre": "San Lorenzo Cuaunecuiltitla",
        "cp": 68518,
        "url": "/localidades/1",
        "instituciones": "/localidades/1/instituciones",
        "actividades": "/localidades/1/actividades"
      }
  @apiSuccessExample {xml} XML
      HTTP/1.1 200 OK
      <?xml version="1.0" encoding="UTF-8"?>
      <localidad>
        <id>1</id>
        <clavelocalidad>1</clavelocalidad>
        <nombre>San Lorenzo Cuaunecuiltitla</nombre>
        <cp>68518</cp>
        <url>/localidades/1</url>
        <instituciones>/localidades/1/instituciones</instituciones>
        <actividades>/localidades/1/actividades</actividades>
      </localidad>
  @apiUse 406
  @apiUse 400
  @apiUse 404
  */

  /**
  @api {get} /localidades/:id/instituciones Solicitud para obtener todas las instituciones ubicadas en cierta localidad especificada por su identificador unico.
  @apiName GetInstitucionesLocalidad
  @apiGroup Localidades

  @apiParam {Number} [id] id Identificador de la localidad
  @apiParam {String} [nombre] Nombre de la institucion, usado para filtrar la respuesta
  @apiParam {Number} [limite=10] Total de resultados, debe ser mayor a 1 y menor 500
  @apiParam {Number} [pagina=1] Utilizado para obtener el offset de los resultados, se usa en conjunto con el parametro limite,debe ser mayor a 1 y menor 30 =1

  @apiSuccess {Object[]} instituciones Lista de instituciones que se ubican en una localidad especificada.
  @apiSuccess {Object} [links]  URL's a datos relacionados con las instituciones.
  @apiSuccess {Number} instituciones.cveInstitucion  Identificador unico de la institucion
  @apiSuccess {String} instituciones.nombre  Nombre de la institucion.
  @apiSuccess {Number} instituciones.url  URL para acceder a la informacion detallada de la institucion.
    @apiSuccess {String} links.anterior URL URL para acceder a la pagina anterior de la respuesta solo se devuelve si la pagina es diferente a 1.
    @apiSuccess {String} links.siguiente URL para acceder a la siguiente pagina de la respuesta.
  @apiSuccessExample {json} JSON:
      HTTP/1.1 200 OK
      {
        "instituciones": [
          {
            "cveinstitucion": "3066085",
            "nombre": "JARDÍN DE NIÑOS CLUB DE LEONES",
            "url": "/instituciones/3066085"
          },
          {
            "cveinstitucion": "3066087",
            "nombre": "ESCUELA PRIMARIA VESPERTINA INDEPENDENCIA",
            "url": "/instituciones/3066087"
          }],
        "links": {
                  "siguiente": "/localidades/5/instituciones?pagina=2&limite=10"
                }
      }
  @apiSuccessExample {xml} XML
      HTTP/1.1 200 OK
      <?xml version="1.0" encoding="UTF-8"?>
        <instituciones siguiente="/localidades/5/instituciones?pagina=2&amp;limite=10">
          <institucion>
            <cveinstitucion>3066085</cveinstitucion>
            <nombre>JARDÍN DE NIÑOS CLUB DE LEONES</nombre>
            <url>/instituciones/3066085</url>
          </institucion>
          <institucion>
            <cveinstitucion>3066087</cveinstitucion>
            <nombre>ESCUELA PRIMARIA VESPERTINA INDEPENDENCIA</nombre>
            <url>/instituciones/3066087</url>
          </institucion>
      </instituciones>
  @apiUse 406
  @apiUse 400
  @apiUse 404
*/

/**
  @api {get} /localidades/:id/instituciones/:cveInstitucion Solicitud para obtener una institucion especificada por cveInstitucion ubicada en una localidad especificada por su identificador unico.
  @apiName GetInstitucionLocalidad
  @apiGroup Localidades

  @apiParam {Number} id Identificador de la localidad.
  @apiParam {Number} cveInstitucion Clave de la institucion

  @apiSuccess {Number} cveInstitucion Clave de la institucion.
  @apiSuccess {String} nombre  Nombre de la institucion
  @apiSuccess {String} url  URL para acceder a la informacion detallada de la institucion.

  @apiSuccessExample {json} JSON:
      HTTP/1.1 200 OK
      "institucion": {
        "id": "3066085",
        "nombre": "JARDÍN DE NIÑOS CLUB DE LEONES",
        "url": "/instituciones/3066085"
      }
  @apiSuccessExample {xml} XML
      HTTP/1.1 200 OK
      <?xml version="1.0" encoding="UTF-8"?>
      <institucion>
        <id>3066085</id>
        <nombre>JARDÍN DE NIÑOS CLUB DE LEONES</nombre>
        <url>/instituciones/3066085</url>
      </institucion>
  @apiUse 406
  @apiUse 400
  @apiUse 404
  */

/**
  @api {get} /localidades/:id/actividades Solicitud para obtener todas las actividades que se realizan en cierta localidad especificada por su identificador unico.
  @apiName GetActividadesLocalidad
  @apiGroup Localidades

  @apiParam {Number} [id] id Identificador de la localidad
  @apiParam {Number} [limite=10] Total de resultados, debe ser mayor a 1 y menor 500
  @apiParam {Number} [pagina=1] Utilizado para obtener el offset de los resultados, se usa en conjunto con el parametro limite,debe ser mayor a 1 y menor 30 =1

  @apiSuccess {Object[]} actividades Lista de actividades que se ejercen en una localidad especificada.
  @apiSuccess {Object} [links]  URL's a datos relacionados con las actividades.
  @apiSuccess {Number} actividades.id  Identificador unico de la actividad.
  @apiSuccess {String} actividades.nombre  Nombre de la actividad.
  @apiSuccess {Number} actividades.url  URL para acceder a la informacion detallada de la actividad.
    @apiSuccess {String} links.anterior URL URL para acceder a la pagina anterior de la respuesta solo se devuelve si la pagina es diferente a 1.
    @apiSuccess {String} links.siguiente URL para acceder a la siguiente pagina de la respuesta.
  @apiSuccessExample {json} JSON:
      HTTP/1.1 200 OK
      {
        "actividades": [
                        {
                          "id": 611112,
                          "nombre": "Escuelas de educación preescolar del sector público",
                          "url": "/actividades/611112"
                        },
                        {
                          "id": 611122,
                          "nombre": "Escuelas de educación primaria del sector público",
                          "url": "/actividades/611122"
                        }],
        "links": {
                  "siguiente": "/localidades/5/actividades?pagina=2&limite=10"
                }
      }
  @apiSuccessExample {xml} XML
      HTTP/1.1 200 OK
      <?xml version="1.0" encoding="UTF-8"?>
      <actividades siguiente="/localidades/5/actividades?pagina=2&amp;limite=10">
        <actividad>
          <id>611112</id>
          <nombre>Escuelas de educación preescolar del sector público</nombre>
          <url>/actividades/611112</url>
        </actividad>
        <actividad>
          <id>611122</id>
          <nombre>Escuelas de educación primaria del sector público</nombre>
          <url>/actividades/611122</url>
        </actividad>
      </actividades>
  @apiUse 406
  @apiUse 400
  @apiUse 404
*/

/**
  @api {post} /localidades Solicitud para crear una localidad nueva
  @apiName PostLocalidad
  @apiGroup Localidades

  @apiParam {Number} claveLocalidad Clave de la localidad de acuerdo al municipio.
  @apiParam {String} nombre Nombre de la localidad nueva.
  @apiParam {Number} cp Codigo postal de la localidad nueva.
  @apiParam {Number} idmunicipio Identificador unico del municipio al que pertenece la localidad nueva.

  @apiSuccessExample
      HTTP/1.1 201 CREATED

  @apiUse 400
*/


/**
  @api {put} /localidades/:id Solicitud para actualizar una localidad
  @apiName PutLocalidad
  @apiGroup Localidades

  @apiParam {String} nombre Nuevo nombre de la localidad.
  @apiParam {Number} cp Nuevo codigo postal de la localidad.
  @apiParam {Number} idmunicipio Identificador unico del municipio al que pertenece la localidad.

  @apiSuccessExample
      HTTP/1.1 201 CREATED

  @apiUse 400
*/

/**
  @api {delete} /localidades/:id Solicitud para eliminar una localidad
  @apiName DeleteLocalidad
  @apiGroup Localidades

  @apiParam {Number} id Identificador unico de la localidad

  @apiSuccessExample
      HTTP/1.1 201 CREATED

  @apiUse 400
*/
