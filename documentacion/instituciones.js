/**
    @apiDefine 406
    @apiError NotAcceptable <code>406</code> No se especifico una cabecera Accept o no es XML o JSON
*/
/**
    @apiDefine 400
    @apiError BadRequest <code>400</code> Alguno de los parametros no tiene un valor valido.
*/

/**
    @apiDefine 404
    @apiError NotFound <code>404</code> El recurso solicitado no se encontro.
*/

/**

  @api {get} /instituciones Solicitud para obtener todas las instituciones
  @apiName GetInstituciones
  @apiGroup Instituciones

  @apiParam {String} [nombre] Nombre de la institucion, usado para filtrar la respuesta
  @apiParam {String} [razonsocial] Razon social de la institucion, usado para filtrar la respuesta
  @apiParam {Number} [limite=100] Total de resultados, debe ser mayor a 1 y menor 500
  @apiParam {Number} [pagina=1] Utilizado para obtener el offset de los resultados, se usa en conjunto con el parametro limite,debe ser mayor a 1 y menor 30

  @apiSuccess {Object[]} instituciones lista de todos las instituciones.
  @apiSuccess {Object} [links]  URL's a las paginas siguientes de la respuesta.
  @apiSuccess {String} instituciones.cveinstitucion Identificador unico de la institucion.
  @apiSuccess {String} instituciones.nombre Nombre de la institucion
  @apiSuccess {String} instituciones.url  URL para acceder a la informacion detallada de la institucion.
  @apiSuccess {String} instituciones.razonsocial Razon social de la institucion.
  @apiSuccess {String} links.siguiente URL para acceder a la siguiente pagina de la respuesta.
  @apiSuccess {String} links.anterior URL para acceder a la pagina anterior de la respuesta solo se devuelve si la pagina es diferente a 1.    
  @apiSuccessExample {json} JSON
      HTTP/1.1 200 OK
      {
        "instituciones": [
            {"cveinstitucion": "3055281",
             "nombre": "ESCUELA PRIMARIA JOSÉ MARÍA MORELOS Y PAVON",
             "razonsocial": "INSTITUTO ESTATAL DE EDUCACIÓN PÚBLICA DE OAXACA",
             "url": "/instituciones/3055281"},
             {"cveinstitucion": "3055286",
              "nombre": "EDUCACIÓN INICIAL",
              "razonsocial": "IEEPO",
              "url": "/instituciones/3055286"}],
        "links":{
              "siguiente": "/instituciones?pagina=2&limite=100",              
        }
      }
  @apiSuccessExample {xml} XML
      HTTP/1.1 200 OK
   <?xml version="1" encoding="UTF-8"?>
    <instituciones siguiente="/instituciones?pagina=2&limite=100" >
      <institucion>
        <cveinstitucion>3055281</cveinstitucion>
        <nombre>ESCUELA PRIMARIA JOSÉ MARÍA MORELOS Y PAVON</nombre>
        <razonsocial>INSTITUTO ESTATAL DE EDUCACIÓN PÚBLICA DE OAXACA</razonsocial>
        <url>/instituciones/3055281</url>
      </institucion>
      <institucion>
        <cveinstitucion>3055286</cveinstitucion>
        <nombre>EDUCACION INICIAL</nombre>
        <razonsocial>IEEPO</razonsocial>
        <url>/instituciones/3055286</url>
      </institucion>
    </instituciones>
  @apiUse 406
  @apiUse 400
 */

/**
 @api {get} /instituciones/:id Solicitud para obtener una institucion
 @apiName GetInstitucion
 @apiGroup Instituciones

 @apiParam {Number} id Identificador unico de la institucion mayor  a 3055281

 @apiSuccess {String} cveinstitucion Identificador unico de la  institucion.
 @apiSuccess {String} nombre Nombre de la institucion
 @apiSuccess {String} personal Indica el numero de personas que  laboran en la institucion, puede ser 1 o un rango, por ejemplo, 3  a 5.
 @apiSuccess {String} telefono Numero telefonico de la institucion
 @apiSuccess {String} email Correo electronico de la institucion
 @apiSuccess {String} web Pagina web de la institucion
 @apiSuccess {String} tipoUniEco Tipo de unidad de la institucion  (fijo, semifijo, movil)
 @apiSuccess {String} fecha_alta Fecha en que se registro la  institucion
 @apiSuccess {String} url  URL para acceder a la informacion  detallada de la institucion.
 @apiSuccess {String} razon_social Razon social de la institucion.

 @apiSuccessExample {json} JSON
     HTTP/1.1 200 OK
     {
       "claveInstitucion": "3055281",
       "nombre": "ESCUELA PRIMARIA JOSÉ MARÍA MORELOS Y PAVON",
       "razon_social": "INSTITUTO ESTATAL DE EDUCACIÓN PÚBLICA DE OAXACA",
       "personal": 1,
       "telefono": null,
       "email": null,
       "web": null,
       "tipoUniEco": "Fijo",
       "fecha_alta": "JULIO 2010",
       "url": "/instituciones/3055281"
     }
 @apiSuccessExample {xml} XML
     HTTP/1.1 200 OK
  <?xml version="1" encoding="UTF-8"?>  
     <institucion>
       <cveinstitucion>3055281</cveinstitucion>
       <nombre>ESCUELA PRIMARIA JOSÉ MARÍA MORELOS Y PAVON</nombre>
       <razon_social>INSTITUTO ESTATAL DE EDUCACIÓN PÚBLICA DE  OAXACA</razonsocial>
       <personal>1</personal>
       <telefono>null</telefono>
       <email>null</null>
       <web>null</web>
       <tipoUniEco>Fijo</tipoUniEco>
       <fecha_alta>JULIO 2010</fecha_alta>
       <url>/instituciones/3055281</url>
     </institucion>

 @apiUse 406
 @apiUse 404
*/

/**
  @api {post} /instituciones Solicitud para crear una nueva institucion
  @apiName PostInstituciones
  @apiGroup Instituciones
  @apiDescription Unicamente se acepta JSON
  @apiParam {Number} cveinstitucion Identificador unico de la institucion, puede entrar en conflicto con alguna otra clave de institucion.
  @apiParam {String} nombre Nombre de la institucion
  @apiParam {String} razonsocial Razon social de la institucion.
  @apiParam {String} perocu Indica el numero de personas que laboran en la institucion, puede ser un numero o un rango.
  @apiParam {String} [telefono] Numero telefonico de la institucion, unicamente se admiten numeros y guiones.
  @apiParam {String} [email] Correo electronico de la institucion
  @apiParam {String} [sitio_web] Direccion de la pagina web de la institucion
  @apiParam {String} [tipounieco] Tipo de unidad de la institucion (fijo, semifijo, movil)
  @apiParam {String} fechaalta Fecha de alta de la institucion al denue
  @apiParam {Number} idactividad Identificador unico de la actividad economica a la que pertenece la institucion
  @apiParam {Object} direccion Objeto direccion de la institucion
  @apiParam {String} direccion.nomvial Nombre de la vialidad
  @apiParam {String} [direccion.letraext] Letra exterior del edificio de la institucion
  @apiParam {String} [direccion.letraint] Letra interior del edificio de la institucion
  @apiParam {Number} direccion.numext Numero exterior del edificio de la institucion
  @apiParam {Number} [direccion.numint] Numero interior del edificio de la institucion
  @apiParam {String} [direccion.edificio] Edificio en el que se ubica la institucion
  @apiParam {String} [direccion.edificioe] Piso del edificio en el que se ubica la institucion
  @apiParam {String} [direccion.tipo_asent] Tipo de asentamiento en el que se ubica la institucion (barrio, ciudad, colonia)
  @apiParam {String} [direccion.nombre_asent] Nombre del asentamiento en que se ubica la institucion
  @apiParam {String} [direccion.tipocencom] Tipo de centro comercial en que se ubica la institucion si es asi.
  @apiParam {String} [direccion.nomcencom] Nombre del centro comercial en que se ubica la institucion.
  @apiParam {String} [direccion.num_local] Numero del local en que se ubica la institucion
  @apiParam {Number} [direccion.ageb] Area superficial de la institucion.
  @apiParam {String} [direccion.manzana] Manzana en que se ubica la institucion
  @apiParam {String} [direccion.latitud] Latitud geografica en que se ubica la institucion
  @apiParam {String} [direccion.longitud] Longitud geografica en que se ubica la institucion
  @apiParam {String} [direccion.idvialidad] Id del tipo de vialidad en que se ubica la institucion
  @apiParam {String} [direccion.idlocalidad] Id de la localidad en que se ubica la institucion
  @apiParam {Object} [direccion.referencias] Referencias geograficas de la institucion
  @apiParam {String} [direccion.referencias.nom_v_e_1] Nombre la vialidad en la esquina 1 de la institucion
  @apiParam {String} [direccion.referencias.nom_v_e_2] Nombre la vialidad en la esquina 2 de la institucion
  @apiParam {String} [direccion.referencias.nom_v_e_3] Nombre la vialidad en la esquina 3 de la institucion
  @apiParam {String} [direccion.referencias.tipo_v_e_1] Identificador del tipo de la vialidad en la esquina 1 de la institucion
  @apiParam {String} [direccion.referencias.tipo_v_e_2] Identificador del tipo de la vialidad en la esquina 2 de la institucion
  @apiParam {String} [direccion.referencias.tipo_v_e_3] Identificador del tipo de la vialidad en la esquina 3 de la institucion

  @apiParamExample {json} Ejemplo de peticion
  {
      "cveinstitucion":"3055299",
      "nombre":"Beembo",
      "razonsocial":"SEP",
      "perocu":"1-5",
      "telefono":"951-111-3331",
      "email":"beembo@beembo.com",
      "sitio_web":"beembo.com.mx",
      "tipounieco":"fijo",
      "fechaalta":"MAYO 2015",
      "idactividad":"611111",
      "direccion":{
        "nomvial":"Benito Juarez",        
        "numext":200,
        "numint":5,                
        "tipo_asent":"Barrio",
        "nombre_asent":"Tecnologico",  
        "ageb":200,        
        "latitud":"16.422926",
        "longitud":"-97.98188",
        "idvialidad":7,
        "idlocalidad":124,
        "referencias.nom_v_e_1":"Margarita Maza",
        "referencias.nom_v_e_2":"Morelos",
        "referencias.tipo_v_e_1":2,
        "referencias.tipo_v_e_2":6
      }
  }

  @apiSuccessExample
      HTTP/1.1 201 CREATED

  @apiUse 400
  @apiUse 406
*/

/**
 @api {put} /instituciones/:id Solicitud para modificar una nueva institucion
 @apiName PutInstituciones
 @apiGroup Instituciones

 @apiDescription Todos los parametros pueden ser opcionales, sin embargo, algunos pueden entrar en conflicto con los datos ya almacenados de la api. Unicamente se acepta JSON.

 @apiParam {Number} id Identificador unico de la institucion.
 @apiParam {String} [nombre] Nombre de la institucion
 @apiParam {String} [razonsocial] Razon social de la institucion.
 @apiParam {String} [perocu] Indica el numero de personas que laboran en la  institucion, puede ser un numero o un rango.
 @apiParam {String} [telefono] Numero telefonico de la institucion, unicamente se  admiten numeros y guiones.
 @apiParam {String} [email] Correo electronico de la institucion
 @apiParam {String} [sitio_web] Direccion de la pagina web de la institucion
 @apiParam {String} [tipounieco] Tipo de unidad de la institucion (fijo,  semifijo, movil)
 @apiParam {String} [fechaalta] Fecha de alta de la institucion al denue
 @apiParam {Number} [idactividad] Identificador unico de la actividad economica a  la que pertenece la institucion
 @apiParam {Object} [direccion] Objeto direccion de la institucion
 @apiParam {String} [direccion.nomvial] Nombre de la vialidad
 @apiParam {String} [direccion.letraext] Letra exterior del edificio de la  institucion
 @apiParam {String} [direccion.letraint] Letra interior del edificio de la  institucion
 @apiParam {Number} [direccion.numext] Numero exterior del edificio de la  institucion
 @apiParam {Number} [direccion.numint] Numero interior del edificio de la  institucion
 @apiParam {String} [direccion.edificio] Edificio en el que se ubica la  institucion
 @apiParam {String} [direccion.edificioe] Piso del edificio en el que se ubica la  institucion
 @apiParam {String} [direccion.tipo_asent] Tipo de asentamiento en el que se  ubica la institucion (barrio, ciudad, colonia)
 @apiParam {String} [direccion.nombre_asent] Nombre del asentamiento en que se  ubica la institucion
 @apiParam {String} [direccion.tipocencom] Tipo de centro comercial en que se  ubica la institucion si es asi.
 @apiParam {String} [direccion.nomcencom] Nombre del centro comercial en que se  ubica la institucion.
 @apiParam {String} [direccion.num_local] Numero del local en que se ubica la  institucion
 @apiParam {Number} [direccion.ageb] Area superficial de la institucion.
 @apiParam {String} [direccion.manzana] Manzana en que se ubica la institucion
 @apiParam {String} [direccion.latitud] Latitud geografica en que se ubica la  institucion
 @apiParam {String} [direccion.longitud] Longitud geografica en que se ubica la  institucion
 @apiParam {String} [direccion.idvialidad] Id del tipo de vialidad en que se  ubica la institucion
 @apiParam {String} [direccion.idlocalidad] Id de la localidad en que se ubica la  institucion
 @apiParam {Object} [direccion.referencias] Referencias geograficas de la  institucion
 @apiParam {String} [direccion.referencias.nom_v_e_1] Nombre la vialidad en la  esquina 1 de la institucion
 @apiParam {String} [direccion.referencias.nom_v_e_2] Nombre la vialidad en la  esquina 2 de la institucion
 @apiParam {String} [direccion.referencias.nom_v_e_3] Nombre la vialidad en la  esquina 3 de la institucion
 @apiParam {String} [direccion.referencias.tipo_v_e_1] Identificador del tipo de  la vialidad en la esquina 1 de la institucion
 @apiParam {String} [direccion.referencias.tipo_v_e_2] Identificador del tipo de  la vialidad en la esquina 2 de la institucion
 @apiParam {String} [direccion.referencias.tipo_v_e_3] Identificador del tipo de  la vialidad en la esquina 3 de la institucion

 @apiParamExample {json} Ejemplo de peticion
 {     

     "telefono":"951-111-3331",
     "email":"beembo2@beembo.com",
     "sitio_web":"beembo2.com.mx",     
     "direccion":{
       "nomvial":"Benito Juarez",        
       "numext":200,
       "numint":5,                              
       "referencias.nom_v_e_2":"Morelos",       
       "referencias.tipo_v_e_2":6
     }
 }

 @apiSuccessExample
     HTTP/1.1 201 CREATED

 @apiUse 400
 @apiUse 406
*/


/**
  @api {delete} /instituciones/:id Solicitud para eliminar una institucion
  @apiName DeleteInstitucion
  @apiGroup Instituciones

  @apiParam {Number} id Identificador unico de la institucion

  @apiSuccessExample
      HTTP/1.1 204 No content

*/
