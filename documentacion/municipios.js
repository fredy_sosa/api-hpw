/**
    @apiDefine 406
    @apiError NotAcceptable <code>406</code> No se especifico una cabecera Accept o no es XML o JSON
*/
/**
    @apiDefine 400
    @apiError BadRequest <code>400</code> Alguno de los parametros no tiene un valor valido.
*/

/**
    @apiDefine 404
    @apiError NotFound <code>404</code> El recurso solicitado no se encontro.
*/

/**

  @api {get} /municipios Solicitud para obtener todos los municipios
  @apiName GetMunicipios
  @apiGroup Municipios
  @apiParam {String} [nombre] Nombre del municipio, usado para filtrar la respuesta
  @apiParam {Number} [limite=50] Total de resultados, debe ser mayor a 1 y menor 500
  @apiParam {Number} [pagina=1] Utilizado para obtener el offset de los resultados, se usa en conjunto con el parametro limite,debe ser mayor a 1 y menor 30

  @apiSuccess {Object[]} municipios lista de todos los municipios.
  @apiSuccess {Object} [links]  URL's a datos relacionados con los municipios.
  @apiSuccess {Number} municipios.id Identificador unico del municipio.
  @apiSuccess {String} municipios.nombre Nombre del municipio
  @apiSuccess {String} municipios.url  URL para acceder a la informacion detallada del municipio.
    @apiSuccess {String} links.siguiente URL para acceder a la siguiente pagina de la respuesta.
    @apiSuccess {String} links.anterior URL para acceder a la pagina anterior de la respuesta.
  @apiSuccessExample {json} JSON
      HTTP/1.1 200 OK
      {
        "municipios": [
                         {"id":1,
                           "nombre":"Abejones",
                           "url":"/municipios/1"},
                         {"id":2,
                           "nombre":"Acatlán de Pérez Figueroa",
                           "url":"/municipios/2"}
                        ],
        "links":{
              "siguiente":"/municipios?pagina=2&limite=50",
              "anterior":"/municipios?pagina=1&limite=50"
        }
      }
  @apiSuccessExample {xml} XML
      HTTP/1.1 200 OK
   <?xml version="1" encoding="UTF-8"?>
    <municipios siguiente="/municipios?pagina=2&limite=50"     anterior="/municipios?pagina=1&limite=50">
      <municipio>
        <id>1</id>
        <nombre>Abejones</nombre>
        <url>/municipios/1</url>
      </municipio>
      <municipio>
        <id>2</id>
        <nombre>Acatlán de Pérez Figueroa</nombre>
        <url>/municipios/2</url>
      </municipio>
    </municipios>
    <links>
      <anterior>/actividades?pagina=1&amp;limite=100</anterior>
      <siguiente>/actividades?pagina=2&amp;limite=100</siguiente>
    </links>
  @apiUse 406
  @apiUse 400
 */


/**
 @api {get} /municipios/:id Solicitud para obtener un municipio
 @apiName GetMunicipio
 @apiGroup Municipios

 @apiParam {Number} id Identificador del municipio entre 1 y 570

 @apiSuccess {Object} [links]  URL's a datos relacionados con el municipio.
 @apiSuccess {Number} id Identificador unico del municipio.
 @apiSuccess {String} nombre Nombre del municipio
 @apiSuccess {String} url  URL para acceder a la informacion detallada del municipio.
 @apiSuccess {String} links.instituciones URL para acceder a las instituciones de todo el municipio.
 @apiSuccess {String} links.localidades URL para acceder a las localidades del municipio.

 @apiSuccessExample {json} JSON
    HTTP/1.1 200 OK
    {"id":1,
     "nombre":"Abejones",
     "url":"/municipios/1",
      "links":{
            "localidades":"/municipios/1/localidades",
            "institucionesr":"/municipios/1/instituciones"
      }
    }

  @apiSuccessExample {xml} XML
      HTTP/1.1 200 OK
   <?xml version="1" encoding="UTF-8"?>
    <municipio>
        <id>1</id>
        <nombre>Abejones</nombre>
        <url>/municipios/1</url>
        <links>
          <localidades>/municipios/1/localidades</localidades>
          <instituciones>/municipios/1/instituciones</instituciones>
        </links>
    </municipio>
  @apiUse 406
  @apiUse 400

*/

/**
 @api {get} /municipios/:id/localidades Solicitud para obtener todas las  localidades de un municipio
 @apiName GetLocalidadesMunicipio
 @apiGroup Municipios

 @apiParam {Number} id Identificador unico del municipio entre 1 y 570
 @apiParam {String} [nombre] Nombre de la localidad para filtrar el resultado
 @apiParam {Number} [limite=10] Total de resultados por pagina, debe ser mayor a  1 y menor 500
 @apiParam {Number} [pagina=1] Utilizado para obtener el offset de los  resultados, se usa en conjunto con el parametro limite,debe ser mayor a 1 y  menor 30 =1

 @apiSuccess {Object[]} localidades lista de las localidades del muncipio.
 @apiSuccess {Object} [links]  URL's a datos relacionados con las localidades.
 @apiSuccess {Number} localidades.id Identificador unico de la localidad.
 @apiSuccess {Number} localidades.clavelocalidad Identificador unico de la  localidad dentro del municipio.
 @apiSuccess {String} localidades.nombre Nombre de la localidad
 @apiSuccess {String} localidades.url  URL para acceder a la informacion  detallada de la localidad.
 @apiSuccess {String} links.siguiente URL para acceder a la siguiente pagina de  la respuesta.
 @apiSuccess {String} links.anterior URL para acceder a la pagina anterior de la  respuesta.
 @apiSuccessExample {json} JSON
     HTTP/1.1 200 OK
     {
       "localidades": [
                        {"id":125,
                          "nombre":"Abejones",
                          "url":"/localidades/125",
                          "clavelocalidad":1}
                       ],
       "links":{
             "siguiente":"/municipios/125/localidades?pagina=2&limite=10",
             "anterior":"municipios/125/localidades?pagina=1&limite=10"
       }
     }
 @apiSuccessExample {xml} XML
     HTTP/1.1 200 OK
  <?xml version="1" encoding="UTF-8"?>
   <localidades siguiente="/municipios/125/localidades?pagina=2&limite=10"      anterior="/municipios/125/localidades?pagina=1&limite=50">
     <localidad>
       <id>125</id>
       <nombre>Abejones</nombre>
       <clavelocalidad>1</clavelocalidad>
       <url>/localidades/125</url>
     </localidad>
   </localidades>
 @apiUse 406
 @apiUse 400
*/

/**
 @api {get} /municipios/:id/localidades/:clavelocalidad Solicitud para obtener   una localidad de un municipio
 @apiName GetLocalidadMunicipio
 @apiGroup Municipios

 @apiParam {Number} id Identificador unico del municipio entre 1 y 570
 @apiParam {Number} clavelocalidad Identificador unico de la localidad dentro del  municipio debe ser mayor a 1


 @apiSuccess {Object} [links]  URL's a datos relacionados con las localidades.
 @apiSuccess {Number} id Identificador unico de la localidad.
 @apiSuccess {Number} clavelocalidad Identificador unico de la localidad dentro  del municipio.
 @apiSuccess {String} nombre Nombre de la localidad
 @apiSuccess {String} URL para acceder a la informacion detallada de la localidad.

 @apiSuccessExample {json} JSON
     HTTP/1.1 200 OK
     {
       "id":125,
       "nombre":"Abejones",
       "url":"/localidades/125",
       "clavelocalidad":1,
     }
 @apiSuccessExample {xml} XML
     HTTP/1.1 200 OK
  <?xml version="1" encoding="UTF-8"?>
     <localidad>
       <id>125</id>
       <nombre>Abejones</nombre>
       <clavelocalidad>1</clavelocalidad>
       <url>/localidades/125</url>
     </localidad>
 @apiUse 406
 @apiUse 400
*/

/**
 @api {get} /municipios/:id/actividades Solicitud para obtener todas las  actividades economicas de un municipio
 @apiName GetActividadesMunicipio
 @apiGroup Municipios

 @apiParam {Number} id Identificador unico del municipio entre 1 y 570
 @apiParam {String} [nombre] Nombre de la actividad para filtrar el resultado
 @apiParam {Number} [limite=10] Total de resultados por pagina, debe ser mayor a  1 y menor 50
 @apiParam {Number} [pagina=1] Utilizado para obtener el offset de los  resultados, se usa en conjunto con el parametro limite,debe ser mayor a 1 y  menor 30

 @apiSuccess {Object[]} actividades Lista de todas las actividades
 @apiSuccess {Number} actividades.id Identificador unico de la actividad.
 @apiSuccess {String} actividades.nombre Nombre de la actividad
 @apiSuccess {String} actividades.URL para acceder a la informacion detallada de  la actividad.

 @apiSuccessExample {json} JSON
     HTTP/1.1 200 OK
     {
       "actividades":[
       {"id":1,
         "nombre":"Escuelas privadas",
         "url":"/actividades/1"},
       {"id":2,
         "nombre":"Escuelas publicas",
         "url":"/actividades/2"}
       ]
     }
 @apiSuccessExample {xml} XML
     HTTP/1.1 200 OK
  <?xml version="1" encoding="UTF-8"?>
   <actividades>
       <actividad>
         <id>1</id>
         <nombre>Escuelas privadas</nombre>
         <url>/actividades/1</url>
       </actividad>
       <actividad>
         <id>2</id>
         <nombre>Escuelas publicas</nombre>
           <url>/actividades/2</url>
       </actividad>
   </actividades>
 @apiUse 406
 @apiUse 400
 @apiUse 404
*/

/**
 @api {get} /municipios/:id/instituciones Solicitud para obtener todas las  instituciones de un municipio
 @apiName GetInstitucionesMunicipio
 @apiGroup Municipios

 @apiParam {Number} id Identificador unico del municipio entre 1 y 570
 @apiParam {String} [nombre] Nombre de la institucion para filtrar el resultado
 @apiParam {Number} [limite=10] Total de resultados por pagina, debe ser mayor a  1 y menor 50
 @apiParam {Number} [pagina=1] Utilizado para obtener el offset de los  resultados, se usa en conjunto con el parametro limite,debe ser mayor a 1 y  menor 30


 @apiSuccess {Object[]} instituciones Lista de todas las instituciones
 @apiSuccess {Number} instituciones.cveinstitucion Identificador unico de la  institucion.
 @apiSuccess {String} instituciones.nombre Nombre de la institucion
 @apiSuccess {String} instituciones.url para acceder a la informacion detallada  de la institucion.

 @apiSuccessExample {json} JSON
     HTTP/1.1 200 OK
     {
       "instituciones":[
       {"cveinstitucion": "3095394",
         "nombre": "ESCUELA SECUNDARIA FEDERAL LUIS DONALDO COLOSIO MURRIETA",
         "url": "/instituciones/3095394"},
       {"cveinstitucion": "3178954",
         "nombre": "COLEGIO RAFAEL RAMIREZ",
         "url": "/instituciones/3178954"}
       ]
     }
 @apiSuccessExample {xml} XML
     HTTP/1.1 200 OK
  <?xml version="1" encoding="UTF-8"?>
   <instituciones>
       <institucion>
         <cveinstitucion>3095394</cveinstitucion>
         <nombre>ESCUELA SECUNDARIA FEDERAL LUIS DONALDO COLOSIO MURRIETA</nombre>
         <url>/instituciones/3095394</url>
       </institucion>
       <institucion>
         <cveinstitucion>3178954</cveinstitucion>
         <nombre>COLEGIO RAFAEL RAMIREZ</nombre>
           <url>/instituciones/3178954</url>
       </institucion>
   </instituciones>
 @apiUse 406
 @apiUse 400
 @apiUse 404
*/


/**
 @api {post} /municipios Solicitud para crear un municipio nuevo
 @apiName PostMunicipio
 @apiGroup Municipios

 @apiParam {String} nombre Nombre del municipio nuevo, cadena de caracteres de longitud maxima 255 caracteres, este endpoint unicamente acepta JSON

 @apiSuccessExample
     HTTP/1.1 201 CREATED

 @apiUse 406
 @apiUse 400
*/

/**
 @api {put} /municipios Solicitud para actualizar un municipio
 @apiName PutMunicipio
 @apiGroup Municipios

 @apiParam {Number} id Identificador unico del municipio
 @apiParam {String} nombre Nuevo nombre del municipio

 @apiSuccessExample
     HTTP/1.1 201 CREATED

 @apiUse 406
 @apiUse 400
*/

/**
 @api {delete} /municipios Solicitud para eliminar un municipio
 @apiName DeleteMunicipio
 @apiGroup Municipios

 @apiParam {Number} id Identificador unico del municipio

 @apiSuccessExample
     HTTP/1.1 201 CREATED

 @apiUse 400
*/

/**
 @api {get} /municipios/:id/localidades/:idlocalidad/actividades Solicitud para obtener todas las actividades economicas de una localidad de un municipio
 @apiName GetActividaesLocalidadesMunicipio
 @apiGroup Municipios

 @apiParam {Number} id Identificador unico del municipio entre 1 y 570
 @apiParam {Number} idlocalidad Identificador unico de la localidad

 @apiSuccess {Object[]} actividades lista de las actividades de la localidad.
 @apiSuccess {Number} actividades.id Identificador unico de la localidad.
 @apiSuccess {String} actividades.nombre Nombre de la localidad
 @apiSuccess {String} actividades.url  URL para acceder a la informacion   detallada de la localidad.
 @apiSuccessExample {json} JSON
     HTTP/1.1 200 OK
     {
       "actividades": [
                        {"id":611112,
                        "nombre": "Escuelas de educación preescolar del sector público",
                        "url": "/actividades/611112"},
                        {"id": 611121,
                        "nombre": "Escuelas de educación primaria del sector privado",
                        "url": "/actividades/611121"}
                       ]
     }
 @apiSuccessExample {xml} XML
     HTTP/1.1 200 OK
  <?xml version="1" encoding="UTF-8"?>
   <actividades>
     <actividad>
       <id>611112</id>
       <nombre>Escuelas de educación preescolar del sector público</nombre>
       <url>/actividades/611112</url>
     </actividad>
     <actividad>
       <id>611121</id>
       <nombre>Escuelas de educación primaria del sector privado</nombre>
       <url>/actividades/611121</url>
     </actividad>
   </actividades>
 @apiUse 406
 @apiUse 404
 @apiUse 400
*/

/**
 @api {get} /municipios/:id/localidades/:idlocalidad/instituciones Solicitud para obtener todas las instituciones de una localidad de un municipio
 @apiName GetInstitucionesLocalidadMunicipio
 @apiGroup Municipios

 @apiParam {Number} id Identificador unico del municipio entre 1 y 570
 @apiParam {Number} idlocalidad Identificador unico de la localidad

 @apiSuccess {Object[]} instituciones lista de las instituciones de la localidad.
 @apiSuccess {String} instituciones.cveinstitucion Identificador unico de la institucion.
 @apiSuccess {String} instituciones.nombre Nombre de la institucion
 @apiSuccess {String} instituciones.url  URL para acceder a la informacion   detallada de la institucion.
 @apiSuccessExample {json} JSON
     HTTP/1.1 200 OK
     {
       "instituciones": [
          {"cveinstitucion": "3095394",
           "nombre": "ESCUELA SECUNDARIA FEDERAL LUIS DONALDO COLOSIO MURRIETA",
           "url": "/instituciones/3095394"},
          {"cveinstitucion": "3178954",
           "nombre": "COLEGIO RAFAEL RAMIREZ",
           "url": "/instituciones/3178954"}
        ]
     }
 @apiSuccessExample {xml} XML
     HTTP/1.1 200 OK
  <?xml version="1" encoding="UTF-8"?>
   <instituciones>
     <institucion>
       <cveinstitucion>3095394</cveinstitucion>
       <nombre>ESCUELA SECUNDARIA FEDERAL LUIS DONALDO COLOSIO MURRIETA</nombre>
       <url>/instituciones/3095394</url>
     </institucion>
     <institucion>
       <cveinstitucion>3178954</cveinstitucion>
       <nombre>COLEGIO RAFAEL RAMIREZ</nombre>
       <url>/instituciones/3178954</url>
     </institucion>
   </instituciones>
 @apiUse 406
 @apiUse 404
*/
