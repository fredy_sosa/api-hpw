/**
    @apiDefine 404
    @apiError NotFound <code>404</code> No se encontró el recurso especificado
*/
/**
    @apiDefine 406
    @apiError NotAcceptable <code>406</code> No se especifico una cabecera Accept o no es XML o JSON
*/
/**
    @apiDefine 400
    @apiError BadRequest <code>400</code> Alguno de los parametros no tiene un valor valido.
*/
/**
  @api {get} /actividades Solicitud para obtener todas actividades economicas
  @apiName GetActividades
  @apiGroup Actividades

  @apiParam {String} [nombre] Nombre de la actividad economica, usado para filtrar la respuesta
  @apiParam {Number} [limite=10] Total de resultados, debe ser mayor a 1 y menor 500
  @apiParam {Number} [pagina=1] Utilizado para obtener el offset de los resultados, se usa en conjunto con el parametro limite,debe ser mayor a 1 y menor 30 =1

  @apiSuccess {Object[]} actividades Lista de las actividades economicas.
  @apiSuccess {Object} [links] URL's a datos relacionados con las actividades.
  @apiSuccess {Number} actividades.id  Identificador unico de la actividad.
  @apiSuccess {String} actividades.nombre  Nombre de la actividad.
  @apiSuccess {Number} actividades.url  URL para acceder a la informacion detallada de la actividad.
    @apiSuccess {String} links.anterior URL URL para acceder a la pagina anterior de la respuesta solo se devuelve si la pagina es diferente a 1.
    @apiSuccess {String} links.siguiente URL para acceder a la siguiente pagina de la respuesta.
  @apiSuccessExample {json} JSON:
      HTTP/1.1 200 OK
      {
        "actividades": [
                        {"id":1,
                          "nombre":"Escuelas privadas",
                          "url":"/actividades/1"},
                        {"id":2,
                          "nombre":"Escuelas publicas",
                          "url":"/actividades/2"}
                        ],
        "links":  {
                    "siguiente": "/actividades?pagina=2&limite=100"
                  }
      }
  @apiSuccessExample {xml} XML
      HTTP/1.1 200 OK
      <?xml version="1" encoding="UTF-8"?>
      <actividades siguiente="/actividades?pagina=2&amp;limite=100">
        <actividad>
          <id>1</id>
          <nombre>Escuelas privadas</nombre>
          <url>/actividades/1</url>
        </actividad>
        <actividad>
          <id>2</id>
          <nombre>Escuelas publicas</nombre>
          <url>/actividades/2</url>
        </actividad>
      </actividades>
  @apiUse 406
  @apiUse 400
  */

  /**
  @api {get} /actividades/:id Solicitud para obtener una actividad economica, especificada por su id
  @apiName GetActividad
  @apiGroup Actividades

  @apiParam {Number} id Identificador de la actividad

  @apiSuccess {Number} idAct  Identificador unico de la actividad.
  @apiSuccess {String} nombre  Nombre de la actividad.
  @apiSuccess {String} url  URL para acceder a la informacion detallada de la actividad.

  @apiSuccessExample {json} JSON:
      HTTP/1.1 200 OK
      { "idact": 611111,
        "nombre": "Escuelas de educación preescolar del sector privado",
        "url": "/actividades/611111"
      }
  @apiSuccessExample {xml} XML
      HTTP/1.1 200 OK
      <?xml version="1.0" encoding="UTF-8"?>
      <actividad>
        <idact>611111</idact>
        <nombre>Escuelas de educación preescolar del sector privado</nombre>
        <url>/actividades/611111</url>
      </actividad>
  @apiUse 406
  @apiUse 400
  @apiUse 404
  */

  /**
  @api {get} /actividades/:id/instituciones Solicitud para obtener todas las instituciones que se dedican a una actividad especificada por su id
  @apiName GetInstitucionesActividad
  @apiGroup Actividades

  @apiParam {Number} [id] id Identificador de la actividad
  @apiParam {String} [nombre] Nombre de la institucion, usado para filtrar la respuesta
  @apiParam {Number} [limite=10] Total de resultados, debe ser mayor a 1 y menor 500
  @apiParam {Number} [pagina=1] Utilizado para obtener el offset de los resultados, se usa en conjunto con el parametro limite,debe ser mayor a 1 y menor 30 =1

  @apiSuccess {Object[]} instituciones Lista de instituciones que se dedican a una actividad especificada.
  @apiSuccess {Object} [links]  URL's a datos relacionados con las instituciones.
  @apiSuccess {Number} instituciones.cveInstitucion  Identificador unico de la institucion
  @apiSuccess {String} instituciones.nombre  Nombre de la institucion.
  @apiSuccess {Number} instituciones.url  URL para acceder a la informacion detallada de la institucion.
    @apiSuccess {String} links.anterior URL URL para acceder a la pagina anterior de la respuesta solo se devuelve si la pagina es diferente a 1.
    @apiSuccess {String} links.siguiente URL para acceder a la siguiente pagina de la respuesta.
  @apiSuccessExample {json} JSON:
      HTTP/1.1 200 OK
      {
        "instituciones": [
                            {
                              "claveinstitucion": "3055286",
                              "nombre": "EDUCACIÓN INICIAL",
                              "url": "/instituciones/3055286"
                            },
                            {
                              "claveinstitucion": "3055292",
                              "nombre": "ESCUELA PREESCOLAR RAMONA MARTÍNEZ ARCOS",
                              "url": "/instituciones/3055292"
                            }
                          ],
        "links": {
                  "siguiente": "/actividades/611112/instituciones?pagina=2&limite=10"
                }
      }
  @apiSuccessExample {xml} XML
      HTTP/1.1 200 OK
      <?xml version="1" encoding="UTF-8"?>
      <instituciones siguiente="/actividades/611112/instituciones?pagina=2&amp;limite=10">
        <institucion>
          <claveinstitucion>3055286</claveinstitucion>
          <nombre>EDUCACIÓN INICIAL</nombre>
          <url>/instituciones/3055286</url>
        </institucion>
        <institucion>
          <claveinstitucion>3055292</claveinstitucion>
          <nombre>ESCUELA PREESCOLAR RAMONA MARTÍNEZ ARCOS</nombre>
          <url>/instituciones/3055292</url>
        </institucion>
      </instituciones>
  @apiUse 406
  @apiUse 400
  @apiUse 404
*/

/**
  @api {get} /actividades/:id/instituciones/:cveInstitucion Solicitud para obtener una institucion especificada su cveInstitucion que ejerce una actividad identificada por su id
  @apiName GetInstitucionActividad
  @apiGroup Actividades

  @apiParam {Number} id Identificador de la actividad
  @apiParam {Number} cveInstitucion Clave de la institucion

  @apiSuccess {Number} cveInstitucion Clave de la institucion.
  @apiSuccess {String} nombre  Nombre de la institucion
  @apiSuccess {String} url  URL para acceder a la informacion detallada de la institucion.

  @apiSuccessExample {json} JSON:
      HTTP/1.1 200 OK
      "institucion": {
        "claveinstitucion": "3055286",
        "nombre": "EDUCACIÓN INICIAL",
        "url": "/instituciones/3055286"
      }
  @apiSuccessExample {xml} XML
      HTTP/1.1 200 OK
      <?xml version="1.0" encoding="UTF-8"?>
      <institucion>
        <claveinstitucion>3055286</claveinstitucion>
        <nombre>EDUCACIÓN INICIAL</nombre>
        <url>/instituciones/3055286</url>
      </institucion>
  @apiUse 406
  @apiUse 400
  @apiUse 404
  */

/**
  @api {post} /actividades Solicitud para crear una actividad nueva
  @apiName PostActividad
  @apiGroup Actividades

  @apiParam {Number} id Identificador unico de la actividad nueva, minimo 714000
  @apiParam {String} nombre Nombre de la actividad nueva, solo acepta JSON

  @apiSuccessExample
      HTTP/1.1 201 CREATED

  @apiUse 400
*/

/**
  @api {put} /actividades/:id Solicitud para actualizar una actividad
  @apiName PutActividad
  @apiGroup Actividades

  @apiParam {Number} id Identificador unico de la actividad.
  @apiParam {String} nombre Nuevo nombre de la actividad.

  @apiSuccessExample
      HTTP/1.1 201 CREATED

  @apiUse 400
*/

/**
  @api {delete} /actividades/:id Solicitud para eliminar una actividad
  @apiName DeleteActividad
  @apiGroup Actividades

  @apiParam {Number} id Identificador unico de la actividad

  @apiSuccessExample
      HTTP/1.1 201 CREATED

  @apiUse 400
*/
